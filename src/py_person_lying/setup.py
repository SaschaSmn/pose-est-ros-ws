from setuptools import find_packages, setup

package_name = 'py_person_lying'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
         ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='Sascha',
    maintainer_email='sascha@none.de',
    description='Trying to detect person lying and marking on map',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'image_node = py_person_lying.pose_detection:main',
            'robo_node = py_person_lying.robo_control:main',
            'marker_node = py_person_lying.rviz_control:main',
            'all_nodes = all_nodes:main'
        ],
    },
)
