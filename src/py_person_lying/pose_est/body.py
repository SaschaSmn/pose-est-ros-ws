from enum import Enum
from collections import deque, Counter
import math
from typing import Optional

integer_convert = lambda x: int(x)
tuple_int_convert = lambda x: (int(x[0]), int(x[1]))
zero_point_filter = lambda x: (x[0], x[1]) != (0, 0)

# ranges : -22 - 22; 23 - 67; 68 - 112; 113 - 157; 158 - -158; -157 - -113; -112 - -68; -67 - -23
range0 = range(-22, 23)
range45 = range(23, 68)
range90 = range(68, 113)
range135 = range(113, 158)
range180_1 = range(158, 181)
range180_2 = range(-180, -157)
range135n = range(-157, -112)
range90n = range(-112, -67)
range45n = range(-67, -22)

'''
How an image works for cv2 depicted by a graph:

cv2: (x,y)
| ------------------> x
|  (0,0) (1,0) (2,0)
|  (0,1) (1,1) (2,1)
|  (0,2) (1,2) (2,2)
\/
y


box xy: is a list with 4 values
keypoints xy: is a list with 17 values that are represented by a tuple of x and y values
(point) values: (x,y)
points : RKNEE, NOSE, ..
'''


class Body:
    """
    Body class.
    Works with the box and points detected from a neural network.
    The points should be in COCO format.
    An example for a neural network that works with this class is the YOLOv8 model
    """

    class Point(Enum):
        """
        Helper class.
        An enum that defines the points and the order of the points
        """
        NOSE = 0
        LEYE = 1
        REYE = 2
        LEAR = 3
        REAR = 4
        LSHOULDER = 5
        RSHOULDER = 6
        LELBOW = 7
        RELBOW = 8
        LWRIST = 9
        RWRIST = 10
        LHIP = 11
        RHIP = 12
        LKNEE = 13
        RKNEE = 14
        LANKLE = 15
        RANKLE = 16

    class BodyPose(Enum):
        """
        Poses of the Body:
        "standing", "sitting", "lying", "kneeling", "upside down", "no essential points", "no full body shown"
        """
        STANDING = 0
        SITTING = 1
        LYING = 2
        KNEELING = 3
        PLANKING = 4
        UPSIDE_DOWN = 5
        NO_ESSENTIAL_POINTS = 6
        NO_FULL_BODY_SHOWN = 7

    class ArmPose(Enum):
        """
        Arm pose of person l|r :
         pose1 : -|- 0|0; pose2: /|\ -45|-45;
         pose3: \|/ 45|45; pose4: \|\ 45|-45;
         pose5: /|/ -45|45; pose6: |°| -90|-90;
         pose7: |.| 90|90
        """
        SIDE_SIDE = 1
        HALF_DOWN_HALF_DOWN = 2
        HALF_UP_HALF_UP = 3
        HALF_UP_HALF_DOWN = 4
        HALF_DOWN_HALF_UP = 5
        DOWN_DOWN = 6
        UP_UP = 7
        NONE = 8

    def __init__(self, xy_points, xy_box, persist_poses: bool = False, persisted_data_count: int = 6,
                 persisted_poses_threshold_percentage: float = 0.50):
        """
        Set points and box
        :param xy_points: List(Tuples); Tuples * 17
            Tuples:= Tuple(int, int)
        :param xy_box: List(int, int, int, int)
        :param persist_poses: bool
            True if last results of poses should be persisted, else False
        :param persisted_data_count: int
            Number of max results to be saved
        :param persisted_poses_threshold_percentage: float; 0.0 to 1.0
            Percentage of elements, that should be the same, for the result of a pose fetching with persisted results
        """
        self._points = list(map(tuple_int_convert, xy_points))
        self.box = list(map(integer_convert, xy_box))

        self.persist_data = persist_poses
        self.persisted_data_count = persisted_data_count
        self.persisted_classifications = deque([], maxlen=self.persisted_data_count)
        if persisted_poses_threshold_percentage < 0.0 or persisted_poses_threshold_percentage > 1.0:
            raise AttributeError("Asserted value out of range")

        self.persisted_poses_threshold_percentage = persisted_poses_threshold_percentage

        self.essential_up_points = [Body.Point.NOSE, Body.Point.LEYE, Body.Point.REYE, Body.Point.LEAR,
                                    Body.Point.REAR]
        self.essential_down_points = [Body.Point.LANKLE, Body.Point.RANKLE]
        self.essential_right_points = [Body.Point.REYE, Body.Point.REAR, Body.Point.RSHOULDER, Body.Point.RELBOW,
                                       Body.Point.RWRIST, Body.Point.RHIP, Body.Point.RKNEE,
                                       Body.Point.RANKLE]
        self.essential_left_points = [Body.Point.LEYE, Body.Point.LEAR, Body.Point.LSHOULDER, Body.Point.LELBOW,
                                      Body.Point.LWRIST, Body.Point.LHIP, Body.Point.LKNEE,
                                      Body.Point.LANKLE]

    def set_points(self, points):
        """
        Set points
        :param points: List(Tuple()); Tuples * 17
            Tuples:= Tuple(int, int)
        """
        self._points = list(map(tuple_int_convert, points))

    def set_points_box(self, points, box):
        """
        Set points and box
        :param points: List(Tuple()); Tuples * 17
            Tuples:= Tuple(int, int)
        :param box: List(int, int, int, int)
        """
        self._points = list(map(tuple_int_convert, points))
        self.box = list(map(integer_convert, box))

    def get_points(self):
        """
        Get the Points. Should be all points that were set
        :return: List(Tuple(int, int))
        """
        return self._points

    def get_body_pose(self, sitting: bool = False, kneeling: bool = False, planking: bool = False) -> str:
        """
        Check the body position of a person.
        Works with is_lying_angle and will make calculations with angles between (body)points.
        Will persist results if persist_poses is enabled and calculate a result considering the last results
        :param sitting: bool
            will be passed to is_lying_angle function
            will lead to an additional result possibility "sitting",
            if disabled -> all possible "sitting" will be "standing"
        :param kneeling: bool
        will be passed to is_lying_angle function
            will lead to an additional result possibility "kneeling",
            if disabled -> all possible "kneeling" will be "standing" or "lying"
        :param planking: bool
        will be passed to is_lying_angle function
            will lead to an additional result possibility "planking",
            if disabled -> all possible "planking" will be "lying"
        """
        return self.get_body_pose_enum(sitting, kneeling, planking).name

    def get_body_pose_enum(self, sitting: bool = False, kneeling: bool = False, planking: bool = False) -> BodyPose:
        """
        Check the body position of a person.
        Works with is_lying_angle and will make calculations with angles between (body)points.
        Will persist results if persist_poses is enabled and calculate a result considering the last results
        :param sitting: bool
            will be passed to is_lying_angle function
            will lead to an additional result possibility SITTING,
            if disabled -> all possible SITTING will be STANDING
        :param kneeling: bool
        will be passed to is_lying_angle function
            will lead to an additional result possibility KNEELING,
            if disabled -> all possible KNEELING will be STANDING or LYING
        :param planking: bool
        will be passed to is_lying_angle function
            will lead to an additional result possibility PLANKING,
            if disabled -> all possible PLANKING will be LYING
        """
        result = self.is_lying_angle_enum(sitting, kneeling, planking)
        if not self.persist_data:
            return result
        else:
            # if the list exceeds the limit, an element from the other side will be removed automatically
            self.persisted_classifications.append(result)
            last_classifications_length = len(self.persisted_classifications)
            if last_classifications_length != self.persisted_data_count:
                return result
            counter = Counter(self.persisted_classifications)
            for classification, cnt in counter.items():
                percentage = cnt / last_classifications_length
                if percentage >= self.persisted_poses_threshold_percentage:
                    return classification
            return result

    def is_lying_angle(self, sitting: bool = False, kneeling: bool = False, planking: bool = False) -> str:
        """
        Check the body position of a person. Works with the angles between (body)points.
        Works with a cascading like technique. If this fails, try this, then this, and so on
        :param sitting: bool
            if True, "sitting" is a possible outcome, else if False "sitting" will default to "standing"
        :param kneeling:  bool
            if True, "kneeling" is a possible outcome, else if False "kneeling" will default to "standing" or "lying"
        :param planking:  bool
        if True, "planking" is a possible outcome, else if False "planking" will default or "lying"
        :return: str
            "standing", "sitting", "lying", "kneeling", "upside down", "no essential points", "no full body shown", "planking"
        """
        return self.is_lying_angle_enum(sitting, kneeling, planking).name

    # only use sitting if you are sure your model is good at detecting points
    def is_lying_angle_enum(self, sitting: bool = False, kneeling: bool = False, planking: bool = False) -> BodyPose:
        """
        Check the body position of a person. Works with the angles between (body)points.
        Works with a cascading like technique. If this fails, try this, then this, and so on
        :param sitting: bool
            if True, SITTING is a possible outcome, else if False SITTING will default to STANDING
        :param kneeling:  bool
            if True, KNEELING is a possible outcome, else if False KNEELING will default to STANDING or LYING
        :param planking:  bool
        if True, PLANKING is a possible outcome, else if False PLANKING will default or LYING
        :return: BodyPose
            STANDING, SITTING, LYING, KNEELING, UPSIDE_DOWN, NO_ESSENTIAL_POINTS, NO_FULL_BODY_SHOWN, PLANKING
        """
        up_points = self.get_filter_values_from_points(
            [Body.Point.LSHOULDER, Body.Point.RSHOULDER] + self.essential_up_points)
        down_points = self.get_filter_values_from_points(
            [Body.Point.LHIP, Body.Point.RHIP, Body.Point.LKNEE, Body.Point.RKNEE] + self.essential_down_points)
        # first check if at least one essential up and down point is present
        # removing the elifs and the whole else section and changing the elifs to ifs makes some sense
        #  it guarantees more results from angle checks, BUT this also leads to the up and down point not being the most
        #  important check. which it is. because e.g. if a persons is lying on their tummy,
        #  the shoulders will be straight. this will result in 'standing' person.
        #  some may argue this is a error i should fix, but i can't.
        #  i rather output lots of NO_FULL_BODY_SHOWN than wrong results
        if up_points and down_points:
            angle = self.get_angle_two_values(down_points[0], up_points[0])
            angle_left_knee_hip = self.get_angle_two_points(Body.Point.LKNEE, Body.Point.LHIP)
            angle_right_knee_hip = self.get_angle_two_points(Body.Point.RKNEE, Body.Point.RHIP)
            angle_left_ankle_knee = self.get_angle_two_points(Body.Point.LANKLE, Body.Point.LKNEE)
            angle_right_ankle_knee = self.get_angle_two_points(Body.Point.RANKLE, Body.Point.RKNEE)
            if angle in range90:
                # detect sitting if enabled, disabled by default. only use if you really trust your model :)
                if (sitting and (angle_left_knee_hip in range0 or angle_right_knee_hip in range0) and (
                        angle_left_knee_hip not in range90 and angle_right_knee_hip not in range90)):
                    if (kneeling and angle_left_ankle_knee in range180_1 or angle_left_ankle_knee in range180_2
                            or angle_right_ankle_knee in range180_1 or angle_right_ankle_knee in range180_2):
                        return Body.BodyPose.KNEELING
                    return Body.BodyPose.SITTING
                if (sitting and (angle_left_knee_hip in range180_1 or angle_left_knee_hip in range180_2
                                 or angle_right_knee_hip in range180_1 or angle_right_knee_hip in range180_2) and (
                        angle_left_knee_hip not in range90 and angle_right_knee_hip not in range90)):
                    if kneeling and angle_left_ankle_knee in range0 or angle_left_ankle_knee in range0:
                        return Body.BodyPose.KNEELING
                    return Body.BodyPose.SITTING
                if (kneeling and
                        ((angle_left_ankle_knee in range0 or angle_left_ankle_knee in range180_1
                          or angle_left_ankle_knee in range180_2) and
                         (angle_right_ankle_knee in range0 or angle_right_ankle_knee in range180_1
                          or angle_right_ankle_knee in range180_2))):
                    return Body.BodyPose.KNEELING
                return Body.BodyPose.STANDING
            # normally range 0, but made ranger bigger for more margin of error
            if angle in range(-25, 25) or angle in range180_1 or angle in range180_2:
                angle_left_elbow_shoulder = self.get_angle_two_points(Body.Point.LELBOW, Body.Point.LSHOULDER)
                angle_right_elbow_shoulder = self.get_angle_two_points(Body.Point.RELBOW, Body.Point.RSHOULDER)
                # if the person is standing but bend over, check if knees to hip are in the angle range
                # if none of the points is detected angle will be -1000 and thus out of range
                if angle_left_knee_hip in range90 or angle_right_knee_hip in range90:
                    if (angle_left_ankle_knee in range0 or angle_left_ankle_knee in range180_1
                            or angle_left_ankle_knee in range180_2 or
                            angle_right_ankle_knee in range0 or angle_right_ankle_knee in range180_1
                            or angle_right_ankle_knee in range180_2):
                        if planking and (angle_left_elbow_shoulder in range90 or angle_right_elbow_shoulder in range90):
                            return Body.BodyPose.PLANKING
                        return Body.BodyPose.KNEELING if kneeling else Body.BodyPose.LYING
                    return Body.BodyPose.STANDING
                if planking and (angle_left_elbow_shoulder in range90 and angle_right_elbow_shoulder in range90):
                    # could add elbow wrist additional check
                    return Body.BodyPose.PLANKING
                return Body.BodyPose.LYING
            if angle in range90n:
                if angle_left_knee_hip in range90 or angle_right_knee_hip in range90:
                    return Body.BodyPose.STANDING
                return Body.BodyPose.UPSIDE_DOWN
        # if only up points
        elif up_points:
            up_angle = None
            head_points = self.get_filter_values_from_points(self.essential_up_points)
            shoulder_points = self.get_filter_values_from_points([Body.Point.LSHOULDER, Body.Point.RSHOULDER])
            # remove nose value if present. because causes bad angles
            nose_value = self.get_value_to_point(Body.Point.NOSE)
            if zero_point_filter(nose_value):
                head_points.remove(nose_value)
            if len(shoulder_points) == 2:
                up_angle = self.get_angle_two_values(shoulder_points[0], shoulder_points[1])
            elif len(head_points) >= 2:
                up_angle = self.get_angle_two_values(head_points[0], head_points[1])
            if up_angle is None:
                return Body.BodyPose.NO_ESSENTIAL_POINTS
            elif up_angle in range0 or up_angle in range180_1 or up_angle in range180_2:
                return Body.BodyPose.STANDING
            elif up_angle in range90 or up_angle in range90n:
                return Body.BodyPose.LYING
        # if only down points
        elif down_points and len(down_points) >= 2:
            down_angle = None
            hip_values = self.get_filter_values_from_points([Body.Point.LHIP, Body.Point.RHIP])
            knee_values = self.get_filter_values_from_points([Body.Point.LKNEE, Body.Point.RKNEE])
            ankle_values = self.get_filter_values_from_points([Body.Point.LANKLE, Body.Point.RANKLE])
            if len(hip_values) == 2:
                down_angle = self.get_angle_two_values(hip_values[0], hip_values[1])
            elif len(knee_values) == 2:
                down_angle = self.get_angle_two_values(knee_values[0], knee_values[1])
            elif len(ankle_values) == 2:
                down_angle = self.get_angle_two_values(ankle_values[0], ankle_values[1])
            if down_angle is None:
                return Body.BodyPose.NO_ESSENTIAL_POINTS
            elif down_angle in range0 or down_angle in range180_1 or down_angle in range180_2:
                return Body.BodyPose.STANDING
            elif down_angle in range90 or down_angle in range90n:
                return Body.BodyPose.LYING
        # no essential points
        else:
            return Body.BodyPose.NO_ESSENTIAL_POINTS
        # fail save method to check other options
        return self.name_body_position_enum()

    def name_body_position(self, ignore_no_full_body: bool = False) -> str:
        """
        Simple algorithm to check the body position of the person.
        is_lying_angle is more complex and works with this method as a fail save
        :param ignore_no_full_body: bool
            if True, ignore that not all bodypoints might be given for calculating a result; default=False
        :return: str
            "standing", "lying", "upside down", "no full body shown"
        """
        return self.name_body_position_enum(ignore_no_full_body).name

    def name_body_position_enum(self, ignore_no_full_body: bool = False) -> BodyPose:
        """
        Simple algorithm to check the body position of the person.
        is_lying_angle is more complex and works with this method as a fail save
        :param ignore_no_full_body: bool
            if True, ignore that not all bodypoints might be given for calculating a result; default=False
        :return: BodyPose
            STANDING, LYING, NO_FULL_BODY_SHOWN, UPSIDE_DOWN
        """
        if not ignore_no_full_body and not self.is_ep_existing():
            return Body.BodyPose.NO_FULL_BODY_SHOWN
        # output only if height greater width
        if not ignore_no_full_body and self.is_body_upsidedown() and not self.is_wbh_box_box_without_arms():
            return Body.BodyPose.UPSIDE_DOWN
        if self.is_wbh_box_box_without_arms():
            return Body.BodyPose.LYING
        else:
            return Body.BodyPose.STANDING

    # wbh = width bigger height
    def is_wbh_choice(self, method) -> bool:
        """
        Function where you can choose the method to calculate the coordinates
        :param method: def -> (int,int,int,int)
            function that returns x_min, y_min, x_max, y_max. Like get_box_box_without_arms
        :return: bool
            True or False; True if the width is bigger than the height and False if the opposite is the case
        """
        x_min, y_min, x_max, y_max = method()
        dx = x_max - x_min  # width
        dy = y_max - y_min  # height
        return dy - dx < 0

    def is_wbh_box(self) -> bool:
        """
        Works with the standard box, to calculate if the box is bigger in width than in height.
        :return: bool
            True or False; True if the width is bigger than the height and False if the opposite is the case
        """
        x1, y1, x2, y2 = self.box
        dx = x2 - x1  # width
        dy = y2 - y1  # height
        return dy - dx < 0

    def is_wbh_box_box_without_arms(self) -> bool:
        """
        Works with the get_box_box_without_arms function, to calculate if the imaginary box from
        (x_min, y_min) to (x_max, y_max), is bigger in width than in height.
        :return: bool
            True or False; True if the width is bigger than the height and False if the opposite is the case
        """
        x_min, y_min, x_max, y_max = self.get_box_box_without_arms()
        dx = x_max - x_min  # width
        dy = y_max - y_min  # height
        return dy - dx < 0

    # only works for arms to width and not height -> height arm distance removal is probably not possible.
    # or is possible but would cut head in half
    # does not work great if the person is lying -> because arms don't matter here
    # -> in this case, the box is cut left/ right based on ankle and eye/ear point
    def get_box_box_without_arms(self) -> tuple[int, int, int, int]:
        """
        Get box coordinates from original box coordinates, but without arms.
        If arms are within the box, the box will be cut short in x range (left to right).
        The outermost value to left or right side will be the min/ max values from the
        get_points_box_without_arms function.
        This won't cut the box in height perspective.
        :return: Tuple(x1, y1, x2, y2)
            A Tuple with the 4 values
        """
        x1, y1, x2, y2 = self.box
        ls_value = self.get_value_to_point(Body.Point.LSHOULDER)
        rs_value = self.get_value_to_point(Body.Point.RSHOULDER)
        range_check = lambda p: p[0] in range(x1, x2) and p[1] in range(y1, y2)
        if range_check(ls_value) and range_check(rs_value):
            x_min, y_min, x_max, y_max = self.get_points_box_without_arms()
            x1 = x_min
            x2 = x_max
        return x1, y1, x2, y2

    def is_wbh_points_box(self) -> bool:
        """
        Works with the get_points_box function, to calculate if the imaginary box from
        (x_min, y_min) to (x_max, y_max), is bigger in width than in height.
        :return: bool
            True or False; True if the width is bigger than the height and False if the opposite is the case
        """
        x_min, y_min, x_max, y_max = self.get_points_box()
        dx = x_max - x_min  # width
        dy = y_max - y_min  # height
        return dy - dx < 0

    def get_points_box(self) -> tuple[int, int, int, int]:
        """
        Get x_min, y_min, x_max, y_max (, respectively x1, y1, x2, y2) coordinates of the box,
        that was made using point values.
        Specifically, the max and min values for x and y dimension of all points detected.
        :return: Tuple(x_min, y_min, x_max, y_max)
             A Tuple with the 4 values
        """
        x_min = self.calc_xy_max_min_values_from_values(0, "min", self._points)
        y_min = self.calc_xy_max_min_values_from_values(1, "min", self._points)
        x_max = self.calc_xy_max_min_values_from_values(0, "max", self._points)
        y_max = self.calc_xy_max_min_values_from_values(1, "max", self._points)
        return x_min, y_min, x_max, y_max

    def is_wbh_points_box_without_arms(self) -> bool:
        """
        Works with the get_points_box_without_arms function, to calculate if the imaginary box from
        (x_min, y_min) to (x_max, y_max), is bigger in width than in height.
        :return: bool
            True or False; True if the width is bigger than the height and False if the opposite is the case
        """
        x_min, y_min, x_max, y_max = self.get_points_box_without_arms()
        dx = x_max - x_min  # width
        dy = y_max - y_min  # height
        return dy - dx < 0

    def get_points_box_without_arms(self) -> tuple[int, int, int, int]:
        """
        Get x_min, y_min, x_max, y_max (, respectively x1, y1, x2, y2) coordinates of the box,
        that was made using point values.
        Specifically, the max and min values for x and y dimension of all points detected without wrists and elbows,
        thus ignoring the arms to improve the algorithm.
        :return: Tuple(x_min, y_min, x_max, y_max)
             A Tuple with the 4 values
        """
        arm_points = [Body.Point.LWRIST, Body.Point.LELBOW, Body.Point.RWRIST, Body.Point.RELBOW]
        points_without_arms = [point for point in Body.Point if point not in arm_points]
        values_without_arms_values = self.get_values_from_points(points_without_arms)
        x_min = self.calc_xy_max_min_values_from_values(0, "min", values_without_arms_values)
        y_min = self.calc_xy_max_min_values_from_values(1, "min", values_without_arms_values)
        x_max = self.calc_xy_max_min_values_from_values(0, "max", values_without_arms_values)
        y_max = self.calc_xy_max_min_values_from_values(1, "max", values_without_arms_values)
        return x_min, y_min, x_max, y_max

    # ep = essential points
    def is_ep_existing(self):
        """
        Check if essential points are existing. Thus meaning if they were detected.
        By that meaning if at least one point of the lower body half and
         at least one point of the upper body half has been detected
        :return: bool
            True or False; True if essential points are existing, else False
        """
        return (self.get_filter_values_from_points(self.essential_up_points) and
                self.get_filter_values_from_points(self.essential_down_points))

    # upside down -> head is lower pos on y-axis than ankles
    def is_body_upsidedown(self) -> bool:
        """
        Returns True if the body is upside down and False if otherwise.
        Very simplistic solution.
        It Works by checking if the minimal value of the upper points is bigger (height),
         than the maximal value of the lower points
        :return: bool
            True or False
        """
        if self.is_ep_existing():
            return (self.calc_xy_max_min_values_from_points(1, "min", self.essential_up_points) >
                    self.calc_xy_max_min_values_from_points(1, "max", self.essential_down_points))
        else:
            return False

    def get_arm_pose(self) -> str:
        """
        Returns pose of person
        l|r : pose1 : -|- 0|0; pose2: /|\ -45|-45;
              pose3: \|/ 45|45; pose4: \|\ 45|-45;
               pose5: /|/ -45|45; pose6: |°| -90|-90;
                pose7: |.| 90|90
        :return: str
            Pose -> p1 to p7
        """
        return self.get_arm_pose_enum().name

    def get_arm_pose_enum(self) -> ArmPose:
        """
        Returns pose of person
        l|r : SIDE_SIDE : -|- 0|0; HALF_DOWN_HALF_DOWN: /|\ -45|-45;
              HALF_UP_HALF_UP: \|/ 45|45; HALF_UP_HALF_DOWN: \|\ 45|-45;
               HALF_DOWN_HALF_UP: /|/ -45|45; DOWN_DOWN: |°| -90|-90;
                UP_UP: |.| 90|90
        :return: ArmPose
            Pose -> SIDE_SIDE, HALF_DOWN_HALF_DOWN, HALF_UP_HALF_UP
                   HALF_UP_HALF_DOWN, HALF_DOWN_HALF_UP , DOWN_DOWN, UP_UP
        """
        angle_left_arm = self.angle_shoulder_elbow_left()
        angle_right_arm = self.angle_shoulder_elbow_right()
        if angle_left_arm is None or angle_right_arm is None:
            return Body.ArmPose.NONE
        if angle_left_arm in range0 and angle_right_arm in range0:
            return Body.ArmPose.SIDE_SIDE
        if angle_left_arm in range45n and angle_right_arm in range45n:
            return Body.ArmPose.HALF_DOWN_HALF_DOWN
        if angle_left_arm in range45 and angle_right_arm in range45:
            return Body.ArmPose.HALF_UP_HALF_UP
        if angle_left_arm in range45 and angle_right_arm in range45n:
            return Body.ArmPose.HALF_UP_HALF_DOWN
        if angle_left_arm in range45n and angle_right_arm in range45:
            return Body.ArmPose.HALF_DOWN_HALF_UP
        if angle_left_arm in range90n and angle_right_arm in range90n:
            return Body.ArmPose.DOWN_DOWN
        if angle_left_arm in range90 and angle_right_arm in range90:
            return Body.ArmPose.UP_UP
        return Body.ArmPose.NONE

    # invert x-axis and y-axis for better angle comparison between arms
    def angle_shoulder_elbow_right(self) -> Optional[int]:
        """
        Get the angle of the right elbow to right shoulder
        :return: Optional[int]
            Angle
        """
        elbow_point = self.get_value_to_point(Body.Point.RELBOW)
        shoulder_point = self.get_value_to_point(Body.Point.RSHOULDER)
        if not zero_point_filter(elbow_point) or not zero_point_filter(shoulder_point):
            return None
        dx = -(elbow_point[0] - shoulder_point[0])
        dy = -(elbow_point[1] - shoulder_point[1])
        angle = int(math.degrees(math.atan2(dy, dx)))
        return angle

    # invert y-axis for better angle comparison between arms
    def angle_shoulder_elbow_left(self) -> Optional[int]:
        """
        Get the angle of the left elbow to left shoulder
        :return: Optional[int]
            Angle
        """
        elbow_point = self.get_value_to_point(Body.Point.LELBOW)
        shoulder_point = self.get_value_to_point(Body.Point.LSHOULDER)
        if not zero_point_filter(elbow_point) or not zero_point_filter(shoulder_point):
            return None
        dx = elbow_point[0] - shoulder_point[0]
        dy = -(elbow_point[1] - shoulder_point[1])
        angle = int(math.degrees(math.atan2(dy, dx)))
        return angle

    # from p2 to p1 -> elbow, shoulder -> from shoulder to elbow angle
    def get_angle_two_points(self, p1: Point, p2: Point) -> Optional[int]:
        """
        always pass the "lower" point as p1 and the "higher" point as p2
        :param p1: Point
            Point point 1
        :param p2: Point
            Point point 2
        :return: int
            Value of the angle between the two points
            Returns -1000 if one of the points has the value of (0,0)
        """
        point1 = self.get_value_to_point(p1)
        point2 = self.get_value_to_point(p2)
        return self.get_angle_two_values(point1, point2)

    def get_angle_two_values(self, p1: tuple[int, int], p2: tuple[int, int]) -> Optional[int]:
        """
        please be sure to always pass the "lower" point as p1 and the "higher" point as p2
        :param p1: Tuple(x,y)
            Value of point 1
        :param p2: Tuple(x,y)
            Value of point 2
        :return: int
            Value of the angle between the two points
            Returns -1000 if one of the points has the value of (0,0)
         """
        if not zero_point_filter(p1) or not zero_point_filter(p2):
            return None
        dx = p1[0] - p2[0]
        dy = p1[1] - p2[1]
        angle = int(math.degrees(math.atan2(dy, dx)))
        return angle

    # calc value of points passed in point list e.g.: [Point.NOSE, Point.LEAR] -> [(0,0), (123,456)]
    def get_values_from_points(self, points: list[Point]) -> list[tuple[int, int]]:
        """
        Get List of values from points
        :param points: List(Point)
            Point enum points
        :return: List(Tuple())
            Points
        """
        return [self.get_value_to_point(point) for point in points]

    # filter points, passed in point list from zero values e.g.: [Point.NOSE, Point.LEAR] -> [(123,456)]
    def get_filter_values_from_points(self, points: list[Point]) -> list[tuple[int, int]]:
        """
        Get list of filtered values, filtered from (0,0) values
        :param points: List(Point)
           Point enum points
        :return: List(Tuple())
            Filtered values
        """
        return list(filter(zero_point_filter, self.get_values_from_points(points)))

    # filter values, passed in value list from zero values e.g.: [(0,0), (123,456)] -> [(123,456)]
    def get_filter_values_from_values(self, points: list[tuple[int, int]]) -> list:
        """
        Get list of filtered values, filtered from (0,0) values
        :param points: List(Tuple())
            Tuples of values of points
        :return: List(Tuple())
            Filtered values
        """
        return list(filter(zero_point_filter, points))

    def calc_xy_max_min_values_from_points(self, xy: int, mm: str, points: list[Point]) -> int:
        """
        Calculate max or min of x or y values from all Point points passed in list as str; e.g.: 0, "max", [Point.NOSE, Point.LEAR]
        :param xy: int
            0 or 1; 0 for x, 1 for y
        :param mm: str
            "max" or "min"
        :param points: List(Point)
            A list of names from Point enum
        :return: int
            max/min x/y value
        """
        filtered_points = self.get_filter_values_from_points(points)
        if filtered_points and mm == "max":
            return max(filtered_points, key=lambda x: x[xy])[xy]
        elif filtered_points and mm == "min":
            return min(filtered_points, key=lambda x: x[xy])[xy]
        return 0

    def calc_xy_max_min_values_from_values(self, xy: int, mm: str, points: list[tuple[int, int]]) -> int:
        """
        Calculate max or min of x or y values from all values passed in the list as tuples; e.g.: 0, "max", [(12,45), (23,56)]
        Also filters from (0,0) values
        :param xy: int
            0 or 1; 0 for x, 1 for y
        :param mm: str
            "max" or "min"
        :param points: List(Tuple())
            A list of (x, y) tuples
        :return: int
            max/min x/y value
        """
        filtered_points = self.get_filter_values_from_values(points)
        if filtered_points and mm == "max":
            return max(filtered_points, key=lambda x: x[xy])[xy]
        elif filtered_points and mm == "min":
            return min(filtered_points, key=lambda x: x[xy])[xy]
        return 0

    def get_filter_all_values(self) -> list:
        """
        Get all values of all points filtered with removing (0,0) values
        :return: List
            All values of points detected
        """
        return list(filter(zero_point_filter, self._points))

    # ex : (850,650) -> Point.NOSE
    def get_point_to_value(self, value: tuple[int, int]) -> Point:
        """
        Get point Point from value
        :param value: Tuple(x,y)
            Value of the Point enum point
        :return: Point;
            Point enum point
        """
        index = self.__get_index_to_value(tuple_int_convert(value))
        return Body.Point(index)

    # ex : Point.RKNEE  -> (850,650)
    def get_value_to_point(self, point: Point) -> tuple[int, int]:
        """
        Get value of point from Point
        :param point: Point
            Point enum point
        :return: Tuple(x,y)
            Value of the Point enum point
        """
        index = point.value
        return self._points[index]

    # ex : (850,650) -> 14
    def __get_index_to_value(self, value: tuple[int, int]) -> int:
        """
        Get Point enum index to the passed value
        :param value: Tuple(x,y)
            Value of the Point enum point
        :return: int
            Index of value in Point enum
        """
        return self._points.index(value)
