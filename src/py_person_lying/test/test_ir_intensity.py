#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
from irobot_create_msgs.msg import IrIntensityVector, IrIntensity
from rclpy.qos import qos_profile_sensor_data
import time


class IrIntensityTest(Node):

    def __init__(self):
        super().__init__("ir_node")
        time.sleep(1)
        rclpy.logging.get_logger("IrIntensityTest").info("IrIntensityTest node started!")

        self.ir_subscriber = self.create_subscription(
            IrIntensityVector,
            "/ir_intensity",
            self.ir_intensity_callback,
            qos_profile_sensor_data
        )

    # TODO
    #  check if every sub message has new readings and list size stays the same or if readings stack up
    #  check if in every reading the order of the sensors is the same
    #  check if messages even arrive and how often they do
    #  check range of value of IrIntensity
    def ir_intensity_callback(self, ir_intensity_vector: IrIntensityVector):
        print("ir detection of 7 sensors")
        print(IrIntensityVector.readings)
        for reading in ir_intensity_vector.readings:
            print(f"sensor: {reading.header.frame_id}")
            print(f"value: {reading.value}")


def main():
    rclpy.init()
    test_node = IrIntensityTest()
    try:
        rclpy.spin(test_node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("Quitting").info("Robo control quit!")
    finally:
        test_node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
