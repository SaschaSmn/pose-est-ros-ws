#!/usr/bin/env python3


import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
from sensor_msgs.msg import Image, CompressedImage
from std_msgs.msg import Float32
from cv_bridge import CvBridge
import cv2
import time


class TestDepthCamera(Node):

    def __init__(self):
        super().__init__("test_depth_camera")
        time.sleep(1)
        self.bridge = CvBridge()
        self.point = (100, 100)
        rclpy.logging.get_logger("TestDepthCamera").info("TestDepthCamera node started!")
        qos_policy = QoSProfile(
            reliability=ReliabilityPolicy.RELIABLE,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        self.camera_sub = self.create_subscription(
            Image,
            "/stereo/depth",
            self.depth_image_callback,
            qos_policy
        )
        self.camera_sub_normal = self.create_subscription(
            Image,
            "/color/video/image",
            self.normal_image_callback,
            qos_policy
        )

    def normal_image_callback(self, msg):
        print("hi2")
        cv_image = self.bridge.imgmsg_to_cv2(msg, desired_encoding="bgr8")
        frame = cv2.resize(cv_image, (640, 640))
        cv2.imshow("normal image cam", frame)
        if cv2.waitKey(1) & 0xFF == ord("q"):
            cv2.destroyAllWindows()

    # TODO test encoding and different cam + if bridge needed
    def depth_image_callback(self, depth_image_raw: Image):
        # resize needs: single channel, 8 bit or floating point image
        # single channel images are greyscale; depth image should be grayscale
        depth_image = self.bridge.imgmsg_to_cv2(depth_image_raw, desired_encoding="passthrough")
        #print(depth_image.shape)
        self.show_frame(depth_image, "depth image bridge passthrough", 1)

        # depth_image = self.bridge.imgmsg_to_cv2(depth_image_raw, desired_encoding="mono16")
        # print(depth_image.shape)
        # self.show_frame(depth_image, "depth image bridge mono16", 1)

        # depth_image = self.bridge.imgmsg_to_cv2(depth_image_raw, desired_encoding="mono8")
        # print(depth_image.shape)
        # self.show_frame(depth_image, "depth image bridge mono8", 1)

        # self.show_frame(depth_image_raw, "depth image raw", 1)
        print(type(depth_image))
        depth_raw = depth_image[self.point]
        print(depth_raw)
        #distance_raw = Float32()
        #distance_raw.data = depth_raw
        #print(f"dist raw: {distance_raw.data}")

        #depth_bridge = depth_image[self.point]
        #distance_bridge = Float32()
        #distance_bridge.data = depth_bridge
        #print(f"dist bridge: {distance_bridge.data}")

    def show_frame(self, frame, frame_name, size):
        # resize frame
        frame = cv2.resize(frame, (int(640 * size), int(640 * size)))
        # show window and quit with "q"
        cv2.imshow(frame_name, frame)
        if cv2.waitKey(1) & 0xFF == ord("q"):
            cv2.destroyAllWindows()
            exit()


def main():
    rclpy.init()
    test_node = TestDepthCamera()
    try:
        rclpy.spin(test_node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("Quitting").info("Robo control quit!")
    finally:
        test_node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
