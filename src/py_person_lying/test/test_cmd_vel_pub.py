#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from math import pi
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy

class TestNode(Node):
    def __init__(self):
        super().__init__("test_node")
        self.movement_publisher = self.create_publisher(
            Twist,
            "/cmd_vel",
            QoSProfile(reliability=ReliabilityPolicy.RELIABLE,history=HistoryPolicy.KEEP_LAST,depth=1)
        )
        self.move()
        #self.timer = self.create_timer(0.3, self.timer_callback)

    def timer_callback(self):
        self.move()

    def move(self):
        movement = Twist()
        movement.angular.z = 1.0
        self.movement_publisher.publish(movement)


def main():
    rclpy.init()
    test_node = TestNode()
    try:
        rclpy.spin(test_node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("Quitting").info("Robo control quit!")
    finally:
        test_node.destroy_node()
        rclpy.shutdown()

if __name__ == "__main__":
    main()