#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.duration import Duration
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
from geometry_msgs.msg import Pose, Quaternion, Point, PoseWithCovarianceStamped
from visualization_msgs.msg import Marker
from tf_transformations import euler_from_quaternion
import math


class TestNode(Node):
    MARKER_DISTANCE = 0.2

    def __init__(self):
        super().__init__("test_node")
        self.counter = 0
        self.pose_data = Pose()
        self.odom_qos_policy = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            history=HistoryPolicy.KEEP_LAST,
            depth=4
        )
        self.odom_subscriber = self.create_subscription(
            PoseWithCovarianceStamped,
            "/amcl_pose",
            self.amcl_pose_callback,
            self.odom_qos_policy
        )
        self.marker_rviz_publisher = self.create_publisher(
            Marker,
            "/visualization_marker",
            10
        )

    def amcl_pose_callback(self, msg):
        self.pose_data = msg.pose.pose
        print("amcl position: " + str(self.pose_data))
        self.counter += 1
        if self.counter == 10:
            self.pub_marker()
            # xdiff = self.pose_data
            # exit()

    def pub_marker(self):
        basic_marker = Marker()
        basic_marker.id = 1
        basic_marker.header.stamp = self.get_clock().now().to_msg()
        basic_marker.header.frame_id = "map"
        basic_marker.scale.x = 0.3
        basic_marker.scale.y = 0.3
        basic_marker.scale.z = 0.3
        basic_marker.lifetime = Duration(seconds=0).to_msg()
        basic_marker.type = 1

        basic_marker.action = 0

        basic_marker.color.r = 0.0
        basic_marker.color.g = 1.0
        basic_marker.color.b = 0.0
        basic_marker.color.a = 1.0
        position = self.pose_data.position
        orientation = self.pose_data.orientation
        basic_marker.pose.orientation = orientation

        quaternion = [orientation.x, orientation.y, orientation.z, orientation.w]
        # Convert quaternion to Euler angles
        (x_roll, y_pitch, z_yaw) = euler_from_quaternion(quaternion)

        # yaw angle is used for orientation
        basic_marker.pose.position.x = position.x + self.MARKER_DISTANCE * math.cos(z_yaw)
        basic_marker.pose.position.y = position.y + self.MARKER_DISTANCE * math.sin(z_yaw)
        basic_marker.pose.position.z = 0.0

        self.marker_rviz_publisher.publish(basic_marker)
        print("published marker")
        print("marker :" + str(basic_marker.pose))
        print("position: " + str(self.pose_data.position))
        print("orientation: " + str(self.pose_data.orientation))


def main():
    rclpy.init()
    test_node = TestNode()
    try:
        rclpy.spin(test_node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("Quitting").info("Robo control quit!")
    finally:
        test_node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
