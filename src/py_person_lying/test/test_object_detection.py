#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import LaserScan
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
import time
import math
from statistics import mean


class TestNode(Node):
    ABSOLUTE_MIN_DISTANCE = 0.2
    OBJECT_AVOIDANCE_DISTANCE = 0.4
    def __init__(self):
        super().__init__("test_node")
        time.sleep(1)
        self.timer = self.create_timer(0.3, self.timer_callback)
        self.laser_data = LaserScan()
        self.counter = 0
        self.qos_policy = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        # Erstellen eines Abonnements für den LaserScan-Topic
        self.laser_subscriber = self.create_subscription(
            LaserScan,
            "/scan",
            self.laser_sub_callback,
            self.qos_policy
        )

    def timer_callback(self):
        object_scan = self.detect_objects(self.laser_data, True)
        grouped_objects = self.group_objects(object_scan)
        avg_objects = self.average_out_object(grouped_objects)
        print("cycle: ")
        print(self.laser_data.angle_min)
        print(self.laser_data.angle_max)
        #print(object_scan)
        print("grouped: "+str(grouped_objects))
        print("avg: "+str(avg_objects))

    # set laser data. this is the laser callback function
    def laser_sub_callback(self, laser_message: LaserScan):
        self.laser_data = laser_message

    # get objects under min distance
    def detect_objects(self, laser_data, filter_180=False):
        objects = []
        if laser_data is not None:
            # for every laserscan get the angle and distance and append to list,
            # if the scan is within a certain distance
            ranges = laser_data.ranges
            for i in range(len(ranges)):
                distance = ranges[i]
                # skip non relevant values
                if (distance == float("inf") or distance > self.OBJECT_AVOIDANCE_DISTANCE or
                        distance < laser_data.range_min or distance > laser_data.range_max):
                    continue
                angle = laser_data.angle_min + i * laser_data.angle_increment
                # auto filter objects behind the robot
                if filter_180 and (angle > 0):
                    continue
                objects.append((angle, distance))
        return objects

    # groups laser scans to object groups by a certain threshold. returns dict
    # e.g., if there is a "hole" in the scans from the angles,
    # bigger than 0.262 (ca. 15 deg) it will match the scans to a new group
    # if the threshold, this might result in only one big object "blob"
    # this method will probably be slow for a lot of scans
    # TODO could also end group if sudden jump in distance, but the distance is already limited by the detection distance
    def group_objects(self, objects, angle_threshold=0.262):
        # Sort the list by angle
        sorted_objects = sorted(objects, key=lambda x: x[0])

        # Group the sorted list by angle difference, considering circular nature
        grouped_objects = {}
        current_group = []

        for i in range(len(sorted_objects)):
            angle, distance = sorted_objects[i]
            prev_angle, _ = sorted_objects[(i - 1) % len(sorted_objects)]

            # Calculate the circular difference between angles
            circular_difference = (angle - prev_angle + math.pi) % (2 * math.pi) - math.pi

            # Start a new group if the circular difference is greater than the threshold
            if abs(circular_difference) > angle_threshold:
                if current_group:
                    grouped_objects[current_group[0][0]] = current_group
                current_group = []

            current_group.append((angle, distance))

        # Handle the last group
        if current_group:
            grouped_objects[current_group[0][0]] = current_group

        return grouped_objects

    def average_out_object(self, grouped_objects):
        average_objects = []
        for values in grouped_objects.values():
            angles, distances = zip(*values)
            average_objects.append((mean(angles), mean(distances)))
        return average_objects


def main():
    rclpy.init()
    test_node = TestNode()
    try:
        rclpy.spin(test_node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("Quitting").info("Robo control quit!")
    finally:
        test_node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
