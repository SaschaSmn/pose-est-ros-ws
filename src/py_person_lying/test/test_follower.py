#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
import time
import math
from statistics import mean, median


class TestNode(Node):
    ABSOLUTE_MIN_DISTANCE = 0.40
    OBJECT_AVOIDANCE_DISTANCE = 0.75

    FOLLOWER_QUIT_DISTANCE = 0.18
    FOLLOWER_STOP_DISTANCE = 0.25
    FOLLOWER_MIN_DISTANCE = 0.26
    FOLLOWER_MAX_DISTANCE = 0.4

    def __init__(self):
        super().__init__("test_node")
        time.sleep(1)
        self.timer = self.create_timer(0.3, self.timer_callback)
        self.laser_data = LaserScan()
        self.qos_policy = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            history=HistoryPolicy.KEEP_LAST,
            depth=1
        )
        # Erstellen eines Abonnements für den LaserScan-Topic
        self.laser_subscriber = self.create_subscription(
            LaserScan,
            "/scan",
            self.laser_sub_callback,
            self.qos_policy
        )

    def timer_callback(self):
        movement = self.calculate_follower_movement(self.laser_data)
        print(f"move: lin x: {movement.linear.x}; ang z: {movement.angular.z}")
        print("--------------")


    # set laser data. this is the laser callback function
    def laser_sub_callback(self, laser_message: LaserScan):
        self.laser_data = laser_message

    def calculate_follower_movement(self, laser_data: LaserScan) -> Twist:
        # filter first invalid values, inf and 0.0; then check if any below break distance, if so break
        # then check if in follow dist and then follow, and stop if to near, should implement some room for dist min/max
        movement = Twist()
        # no laser data
        if laser_data.angle_increment == 0.0:
            return movement

        # objects under MAX_FOLLOWER_DISTANCE with an angle and distance and only from the front of the robot
        objects = self.detect_objects(laser_data, True, self.FOLLOWER_MAX_DISTANCE)
        distances = [x[1] for x in objects]
        if distances:
            print(f"min dist: {min(distances)}")
        # if any distance below quit distance (0.1) is recognized, quit and change mode back to robo control
        if any(distance[1] <= self.FOLLOWER_QUIT_DISTANCE for distance in objects):
            #self.state = RoboControl.State.POSE_CONTROL
            print("mode to robocontrol")
            return movement

        # if any distance below stop distance (0.15) is recognized, stop robo
        if any(distance[1] <= self.FOLLOWER_STOP_DISTANCE for distance in objects):
            print("stop")
            return movement

        # filter so only between 0.2 and 0.3 dist remain
        objects = list(filter(lambda x: x[1] >= self.FOLLOWER_MIN_DISTANCE, objects))
        if not objects:
            print("no objects")
            return movement

        objects = self.average_out_object(self.group_objects(objects))
        print(objects)
        # slow movement; driving angle is the first object detected angle
        movement.linear.x = 0.05
        movement.angular.z = objects[0][0] + math.pi / 2
        return movement

    # get objects under min distance
    def detect_objects(self, laser_data: LaserScan, filter_180: bool = False,
                       filter_distance: float = OBJECT_AVOIDANCE_DISTANCE) -> list:
        objects = []
        if laser_data.angle_increment == 0.0:
            return objects
        if laser_data is not None:
            # for every laserscan get the angle and distance and append to list,
            # if the scan is within a certain distance
            ranges = laser_data.ranges
            # filter objects behind the robot
            if filter_180:
                ranges = ranges[0:int((0 - laser_data.angle_min) / laser_data.angle_increment) + 2]
            for i in range(len(ranges)):
                angle = laser_data.angle_min + i * laser_data.angle_increment
                distance = ranges[i]
                # skip non relevant values
                if (distance == float("inf") or distance > filter_distance or
                        distance < laser_data.range_min or distance > laser_data.range_max):
                    continue
                objects.append((angle, distance))
        return objects

    # groups laser scans to object groups by a certain threshold. returns dict
    # e.g., if there is a 'hole' in the scans from the angles,
    # bigger than 0.262 (ca. 15 deg) it will match the scans to a new group
    # if the threshold, this might result in only one big object "blob'
    # this method will probably be slow for a lot of scans
    # TODO could also end group if sudden jump in distance, but the distance is already limited by the detection distance
    def group_objects(self, objects: list, angle_threshold: float = 0.262) -> dict:
        # Sort the list by angle
        sorted_objects = sorted(objects, key=lambda x: x[0])

        # Group the sorted list by angle difference, considering circular nature
        grouped_objects = {}
        current_group = []

        for i in range(len(sorted_objects)):
            angle, distance = sorted_objects[i]
            prev_angle, _ = sorted_objects[(i - 1) % len(sorted_objects)]

            # Calculate the circular difference between angles
            circular_difference = (angle - prev_angle + math.pi) % (2 * math.pi) - math.pi

            # Start a new group if the circular difference is greater than the threshold
            if abs(circular_difference) > angle_threshold:
                if current_group:
                    grouped_objects[current_group[0][0]] = current_group
                current_group = []

            current_group.append((angle, distance))

        # Handle the last group
        if current_group:
            if abs(((sorted_objects[0][0] - sorted_objects[-1][0] + math.pi) % (
                    2 * math.pi) - math.pi)) < angle_threshold and grouped_objects:
                first_group = grouped_objects.get(sorted_objects[0][0])
                grouped_objects[sorted_objects[0][0]] = first_group + current_group
            else:
                grouped_objects[current_group[0][0]] = current_group

        return grouped_objects

    # calculate median angle and average distance of grouped scans, so there are only single points of an object left
    # median of angle will fail, if two angles exactly between -pi and +pi
    def average_out_object(self, grouped_objects: dict) -> list:
        average_objects = []
        for values in grouped_objects.values():
            angles, distances = zip(*values)
            average_objects.append((median(angles), mean(distances)))
        return average_objects

    def calculate_object_group_length(self, group: list) -> float:
        angle1, distance1 = group[0]
        angle2, distance2 = group[-1]
        # Calculate length of object using trigonometry
        length = math.sqrt(
            (distance2 * math.cos(angle2) - distance1 * math.cos(angle1)) ** 2 +
            (distance2 * math.sin(angle2) - distance1 * math.sin(angle1)) ** 2
        )
        return length
    
def main():
    rclpy.init()
    test_node = TestNode()
    try:
        rclpy.spin(test_node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("Quitting").info("Robo control quit!")
    finally:
        test_node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
