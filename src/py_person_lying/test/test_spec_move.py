#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist
from math import pi
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy

class TestNode(Node):
    def __init__(self):
        super().__init__("test_node")
        reliable_qos_policy = QoSProfile(
            history=HistoryPolicy.KEEP_LAST,
            depth=6,
            reliability=ReliabilityPolicy.RELIABLE
        )
        self.twist_robo_control_controller_publisher = self.create_publisher(
            Twist,
            "/specified_movement",
            reliable_qos_policy
        )
        for _ in range(0,4):
            movement = Twist()
            movement.angular.z = 1.0
            self.twist_robo_control_controller_publisher.publish()


def main():
    rclpy.init()
    test_node = TestNode()
    try:
        rclpy.spin(test_node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("Quitting").info("Robo control quit!")
    finally:
        test_node.destroy_node()
        rclpy.shutdown()

if __name__ == "__main__":
    main()