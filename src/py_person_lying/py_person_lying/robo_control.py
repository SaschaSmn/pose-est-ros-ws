#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy, qos_profile_sensor_data
from std_msgs.msg import String, Float32
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from irobot_create_msgs.msg import IrIntensityVector, IrIntensity

from enum import Enum
from statistics import mean, median
import time
import math

from src.py_person_lying.pose_est.body import Body


class RoboControl(Node):
    ABSOLUTE_MIN_DISTANCE = 0.40
    OBJECT_AVOIDANCE_DISTANCE = 0.75
    IR_DISTANCE_MIN = 175  # smaller = bigger dist; bigger = later object detection

    FOLLOWER_QUIT_DISTANCE = 0.18
    FOLLOWER_STOP_DISTANCE = 0.29  # should always be a little smaller than FOLLOWER_MIN_DISTANCE
    FOLLOWER_MIN_DISTANCE = 0.30
    FOLLOWER_MAX_DISTANCE = 0.42

    MAX_ANGULAR_VELOCITY = 1.9  # rad
    MIN_LINEAR_SPEED = 0.03
    MAX_LINEAR_SPEED = 0.2
    TURNING_ANGLE = math.pi / 8

    class State(Enum):
        EXPLORE = 0
        POSE_CONTROL = 1
        DRIVE_TO_BOX = 2
        STAND = 3
        FOLLOWER = 4
        SPEC_MOVE = 5
        EXIT = 6

    def __init__(self):
        super().__init__("robot_node")
        time.sleep(1)
        rclpy.logging.get_logger("RoboControl").info("RoboControl node started!")
        self.timer = self.create_timer(0.3, self.timer_callback)

        self.state = RoboControl.State.EXPLORE
        self.poses = []
        self.spec_moves = []
        self.laser_data = LaserScan()
        self.box_data = []
        self.ir_messages = []
        self.last_turn_direction = 1  # 1 = left; -1 = right
        # adjust the speed of robo to the robo distance to an object
        self.MAPPED_SPEED = lambda x: ((x - self.ABSOLUTE_MIN_DISTANCE) / (
                self.OBJECT_AVOIDANCE_DISTANCE - self.ABSOLUTE_MIN_DISTANCE)) * (
                                              self.MAX_LINEAR_SPEED - self.MIN_LINEAR_SPEED) + self.MIN_LINEAR_SPEED

        self.init_communications()

    def init_communications(self):
        best_effort_qos_policy = QoSProfile(
            history=HistoryPolicy.KEEP_LAST,
            depth=1,
            reliability=ReliabilityPolicy.BEST_EFFORT
        )
        reliable_qos_policy = QoSProfile(
            history=HistoryPolicy.KEEP_LAST,
            depth=6,
            reliability=ReliabilityPolicy.RELIABLE
        )

        self.movement_publisher = self.create_publisher(
            Twist,
            "/cmd_vel",
            best_effort_qos_policy
        )
        self.marker_rviz_controller_publisher = self.create_publisher(
            String,
            "/rviz_marker",
            reliable_qos_policy
        )

        self.laser_subscriber = self.create_subscription(
            LaserScan,
            "/scan",
            self.laser_sub_callback,
            best_effort_qos_policy
        )
        self.ir_subscriber = self.create_subscription(
            IrIntensityVector,
            "/ir_intensity",
            self.ir_intensity_callback,
            qos_profile_sensor_data
        )
        self.image_process_mode_subscriber = self.create_subscription(
            String,
            "/mode",
            self.mode_callback,
            reliable_qos_policy
        )
        self.image_process_box_align_subscriber = self.create_subscription(
            String,
            "/box_align",
            self.box_align_callback,
            QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT)
        )
        self.image_process_arm_pose_subscriber = self.create_subscription(
            String,
            "/arm_pose",
            self.arm_pose_callback,
            reliable_qos_policy
        )
        # not needed anymore, but if further additions are planned, leave it here
        self.image_process_twist_subscriber = self.create_subscription(
            Twist,
            "/specified_movement",
            self.specified_move_callback,
            reliable_qos_policy
        )

    # start timer
    def start_timer(self, timer):
        self.timer = timer

    def stop_timer(self):
        """
        Returns the old timer
        """
        old_timer = self.timer
        self.timer = None
        return old_timer

    # 0.3 sec timer
    def timer_callback(self):
        if self.state != RoboControl.State.EXPLORE:
            print(f"{self.state.name}")
        movement = Twist()
        if self.state == RoboControl.State.EXPLORE:
            movement = self.calculate_explore_movement(self.laser_data, True)
        if self.state == RoboControl.State.POSE_CONTROL:
            movement = self.calculate_pose_movement(self.poses)
        if self.state == RoboControl.State.DRIVE_TO_BOX:
            movement = self.calculate_align_movement(self.box_data)
        if self.state == RoboControl.State.STAND:
            pass
        if self.state == RoboControl.State.SPEC_MOVE:
            movement = self.specified_movement(self.spec_moves)
        if self.state == RoboControl.State.FOLLOWER:
            movement = self.calculate_follower_movement(self.laser_data)
        print(f"move: lin x: {movement.linear.x}; ang z: {movement.angular.z}")
        self.movement_publisher.publish(movement)

    # laser callback
    def laser_sub_callback(self, laser_message: LaserScan):
        self.laser_data = laser_message

    # ir callback
    def ir_intensity_callback(self, ir_intensity_vector: IrIntensityVector):
        self.ir_messages = ir_intensity_vector.readings

    # mode callback for the switch mode from ImageProcess class
    def mode_callback(self, mode_message: String):
        if mode_message.data in [x.name for x in RoboControl.State]:
            self.state = RoboControl.State[mode_message.data]
            rclpy.logging.get_logger("RoboControl").info(f"Switched mode to: {self.state.name}")
            # clean poses after leaving pose_control
            if self.poses and self.state == RoboControl.State.EXPLORE:
                self.poses = []
        else:
            rclpy.logging.get_logger("RoboControl").info(f"Send wrong message for mode switch: {mode_message.data}")
        if self.state == RoboControl.State.EXIT:
            raise SystemExit

    # box callback from ImageProcess class for box data, if the person is lying and robo is set to align
    def box_align_callback(self, box_message: String):
        substring_list = box_message.data.split(",")
        int_list = list(map(int, substring_list))
        self.box_data = int_list
        # print(f"box align callback, box: {int_list}")

    # arm pose callback for pose control from ImageProcess class
    def arm_pose_callback(self, arm_pose_message: String):
        if arm_pose_message.data in [x.name for x in Body.ArmPose]:
            self.poses.append(Body.ArmPose[arm_pose_message.data])
        else:
            rclpy.logging.get_logger("RoboControl").info(f"Send wrong message for arm pose: {arm_pose_message.data}")

    # callback if special movement was called.
    # makes it possible to call/pub movement from different node
    def specified_move_callback(self, movement_message: Twist):
        self.spec_moves.append(movement_message)

    # calculate movement in explore mode from laserscan and ir scan
    # pre_filter param, will be handed to the detect_objects function, so that only objects from the 180° front of robo
    #  will be detected
    def calculate_explore_movement(self, laser_data: LaserScan, pre_filter: bool = False) -> Twist:
        # if laser_data is not set yet. angle increment should always be set is laser data was published
        movement = Twist()
        if laser_data.angle_increment == 0.0:
            return movement

        objects = self.detect_objects(laser_data, pre_filter, self.OBJECT_AVOIDANCE_DISTANCE)

        # filter so only objects from robo front are left.
        # be sure items are sorted
        if not pre_filter:
            objects = sorted(list(filter(lambda angle: angle[0] < 0, objects)))

        # if no objects are found, return straight movement
        if not objects:
            movement.linear.x = self.MAX_LINEAR_SPEED
            return movement

        # if an object too close, just turn until nothing is in front of robo anymore
        # ir and laser min dist check
        if any(dist.value > self.IR_DISTANCE_MIN for dist in self.ir_messages):
            print("ir dist min")
            movement.linear.x = 0.0
            if any(dist.value > self.IR_DISTANCE_MIN for dist in self.ir_messages[0:3]):
                self.last_turn_direction = -1
            elif any(dist.value > self.IR_DISTANCE_MIN for dist in self.ir_messages[4:6]):
                self.last_turn_direction = 1
            movement.angular.z = self.TURNING_ANGLE * self.last_turn_direction
            return movement
        elif any(distance[1] <= self.ABSOLUTE_MIN_DISTANCE for distance in objects):
            print("laser dist min")
            movement.linear.x = 0.0
            movement.angular.z = self.TURNING_ANGLE * self.last_turn_direction
            return movement

        drive_direction_angular_z = 0.0
        # lambda for splitting objects in two different lists
        right_side_objects = sorted(list(filter(lambda x: x[0] <= -math.pi / 2, objects)), reverse=True)
        left_side_objects = sorted(list(filter(lambda x: x[0] > -math.pi / 2, objects)))

        # so that drive in the other direction from the angle of the object
        drive_convert = lambda x: (x + math.pi) if x <= -math.pi / 2 else -(abs(x))
        # for calculating the index in ranges of laser_scan (so the index of the scan in the scan list)
        index_ranges_calc = lambda x: int((x - laser_data.angle_min) / laser_data.angle_increment)

        angle_nearest_object = None
        # check if objects are only on one side or both
        if right_side_objects and left_side_objects:
            angle_right = right_side_objects[0][0]
            angle_left = left_side_objects[0][0]
            # (smallest = > near 0)
            smallest_angles = [drive_convert(angle_right), drive_convert(angle_left)]
            # average of the smallest angles from the left and right side
            drive_direction_angular_z = mean(smallest_angles)

            # calc nearest dist to object for robo speed calc
            dist_right = laser_data.ranges[index_ranges_calc(angle_right)]
            dist_left = laser_data.ranges[index_ranges_calc(angle_left)]
            angle_nearest_object = angle_right if dist_right >= dist_left else angle_left
        elif right_side_objects:
            angle_nearest_object = right_side_objects[0][0]
            drive_direction_angular_z = drive_convert(angle_nearest_object)
        elif left_side_objects:
            angle_nearest_object = left_side_objects[0][0]
            drive_direction_angular_z = drive_convert(angle_nearest_object)

        dist_object = laser_data.ranges[index_ranges_calc(angle_nearest_object)]
        movement.linear.x = self.MAPPED_SPEED(
            dist_object) if angle_nearest_object is not None else self.MAX_LINEAR_SPEED
        movement.angular.z = drive_direction_angular_z
        return movement

    # calculate movement for pose control
    def calculate_pose_movement(self, poses) -> Twist:
        movement = Twist()
        if not poses:
            return movement
        pose = poses[0]
        if pose == Body.ArmPose.HALF_DOWN_HALF_DOWN:
            movement.linear.x = 0.2
        elif pose == Body.ArmPose.HALF_UP_HALF_UP:
            pass
        elif pose == Body.ArmPose.HALF_UP_HALF_DOWN:
            movement.angular.z = -0.1
        elif pose == Body.ArmPose.HALF_DOWN_HALF_UP:
            movement.angular.z = 0.1
        # idle pose p6
        elif pose == Body.ArmPose.DOWN_DOWN:
            pass
        elif pose == Body.ArmPose.UP_UP:
            self.poses = []
            self.state = RoboControl.State.FOLLOWER
            return movement
        poses.pop(0)
        return movement

    # could lead to wrong alignment *if* box data would not get updated
    # calc robo box/person align movement
    def calculate_align_movement(self, box, ix: int = 640, iy: int = 640) -> Twist:
        movement = Twist()
        # if call is placed before box is set
        if not box:
            print("no box")
            return movement
        # check no blind driving straight to box -> if too close to object -> turn
        if any(dist.value > self.IR_DISTANCE_MIN for dist in self.ir_messages):
            print("ir dist min box")
            if any(dist.value > self.IR_DISTANCE_MIN for dist in self.ir_messages[0:3]):
                self.last_turn_direction = -1
            elif any(dist.value > self.IR_DISTANCE_MIN for dist in self.ir_messages[4:6]):
                self.last_turn_direction = 1
            movement.angular.z = self.TURNING_ANGLE * self.last_turn_direction
            return movement

        x1, y1, x2, y2 = box
        len_box_x = abs(x2 - x1)
        len_box_y = abs(y2 - y1)
        # smaller number means, the box has to cover less space of the image
        len_detection_x = int(ix / 6)
        len_detection_y = int(iy / 2.75)
        center_box_x = (x1 + x2) / 2
        error_box_x = int(int(ix / 2) - center_box_x)  # calculate error robo camera center and bounding box center

        # error more than 30px turn robo
        if abs(error_box_x / 10) > 3.0:
            movement.angular.z = 0.1 if error_box_x > 0 else -0.1
        # drive nearer to box, if box px to small (person too far away)
        if (ix - len_box_x) > len_detection_x or (iy - len_box_y) > len_detection_y:
            movement.linear.x = 0.1
        # if close to an object: collect data (person length, angle and dist) and pub to marker
        elif abs(error_box_x / 10) < 3.0:
            objects = self.detect_objects(self.laser_data, True, 1.4)
            grouped_objects = self.group_objects(objects, 0.175)  # 0.175 ≈ 10 grad
            angle_mid = -math.pi / 2  # angle of front middle of robo
            angle_diff = math.pi / 16  # difference angle range from robo front middle from side to side
            key = None
            # get the object group of the person
            for angle, angle_dist_pairs in grouped_objects.items():
                # check angle in range
                if any(angle_mid - angle_diff < angle_dist_pair[0] <= angle_mid + angle_diff for angle_dist_pair in
                       angle_dist_pairs):
                    key = angle
                    break

            person_group = grouped_objects.get(key, [])  # get the group with distances, else empty list
            person_length = self.calculate_object_group_length(person_group)  # length of person
            person_angle = self.calculate_object_group_yaw(person_group)  # angle of person lying
            person_dist = person_group[int(len(person_group) / 2)][1] if person_group else 0.0  # dist of person to robo
            print("Trying to set marker, person full in frame")
            person_marker_msg = String()
            person_marker_msg.data = f"{person_dist},{person_angle},{person_length}"
            self.marker_rviz_controller_publisher.publish(person_marker_msg)

            # no need to turn robo, can just switch mode
            # if turn and person in image, automatic switch in ImageProcess node to fall_detect/explore mode
            # manually call mode callback. just for cli output mode switch
            msg = String()
            msg.data = "EXPLORE"
            self.mode_callback(msg)
            print("sleeping 2s zzzzZZ")
            time.sleep(2)  # sleep so time for the marker placement (technically should not be necessary)
        return movement

    # spec move call (just removes the first spec move)
    def specified_movement(self, spec_moves):
        movement = Twist()
        if not spec_moves:
            return movement
        movement = spec_moves.pop(0)
        return movement

    # !! only works in free room !! objects near robo might distract !! place robo in free space !!
    # TODO further improvement:
    #  could include image callback deactivate for image process if in follower mode to spare resources
    #  but either would need to include signal in image process to stop follower mode or stay in robo control mode
    def calculate_follower_movement(self, laser_data: LaserScan) -> Twist:
        # filter first invalid values, inf and 0.0; then check if any below break distance, if so break
        # then check if in follow dist range and then follow, and stop if to near
        movement = Twist()
        # no laser data
        if laser_data.angle_increment == 0.0:
            return movement

        # objects under MAX_FOLLOWER_DISTANCE with an angle and distance and only from the front of the robot
        objects = self.detect_objects(laser_data, True, self.FOLLOWER_MAX_DISTANCE)

        # if any distance below FOLLOWER_QUIT_DISTANCE distance is recognized, quit and change mode back to robo control
        if any(distance[1] <= self.FOLLOWER_QUIT_DISTANCE for distance in objects):
            self.state = RoboControl.State.POSE_CONTROL
            return movement

        # if any distance below FOLLOWER_STOP_DISTANCE distance is recognized, stop robo
        if any(distance[1] <= self.FOLLOWER_STOP_DISTANCE for distance in objects):
            return movement

        # filter so only between FOLLOWER_MIN_DISTANCE and FOLLOWER_MAX_DISTANCE dist remain
        objects = list(filter(lambda x: x[1] >= self.FOLLOWER_MIN_DISTANCE, objects))

        # if no scan meets criteria, stop robo (e.g.: only scan bigger FOLLOWER_MAX_DISTANCE
        #  or between FOLLOWER_STOP_DISTANCE and FOLLOWER_MIN_DISTANCE (unrealistic))
        if not objects:
            return movement

        # follow midpoint of the object
        objects = self.average_out_object(self.group_objects(objects))
        # slow movement; driving angle is the first object detected angle
        movement.linear.x = 0.1
        movement.angular.z = objects[0][0] + math.pi / 2
        return movement

    # get objects under min distance (essentially looks if a scan distance is below a distance)
    def detect_objects(self, laser_data: LaserScan, filter_180: bool = False,
                       filter_distance: float = OBJECT_AVOIDANCE_DISTANCE) -> list:
        objects = []
        if laser_data is not None:
            # if the scan is within a certain distance, get the angle and distance and append to list
            ranges = laser_data.ranges
            # filter objects behind the robot
            if filter_180:
                ranges = ranges[0:int((0 - laser_data.angle_min) / laser_data.angle_increment) + 2]
            for i in range(len(ranges)):
                angle = laser_data.angle_min + i * laser_data.angle_increment
                distance = ranges[i]
                # skip non relevant values
                if (distance == float("inf") or distance > filter_distance or
                        distance < laser_data.range_min or distance > laser_data.range_max):
                    continue
                objects.append((angle, distance))
        return objects

    # groups laser scans to object groups by a certain threshold. returns dict (e.g.: 2.16: ((2.16, 1.0), (2.18,1.1)) )
    # e.g., if there is a 'hole' in the scans from the angles,
    # bigger than 0.262 (ca. 15 deg) it will match the scans to a new group
    # if the threshold is too big, this might result in only one big object group
    # this method could be slow for a lot of scans, but was optimized to the best of my knowledge
    # min group angle_threshold is 0.008714509196579456
    # TODO further improvement:
    #  could also end group if sudden jump in distance, but the distance is already limited by the detection distance
    def group_objects(self, objects: list, angle_threshold: float = 0.262) -> dict:
        # Sort the list by angle
        sorted_objects = sorted(objects, key=lambda x: x[0])
        # Group the sorted list by angle difference, considering circular angles
        grouped_objects = {}
        current_group = []
        for i in range(len(sorted_objects)):
            angle, distance = sorted_objects[i]
            prev_angle, _ = sorted_objects[(i - 1) % len(sorted_objects)]
            # Calculate the circular difference between angles
            circular_difference = (angle - prev_angle + math.pi) % (2 * math.pi) - math.pi
            # Start a new group if the circular difference is bigger than the threshold
            if abs(circular_difference) > angle_threshold:
                if current_group:
                    grouped_objects[current_group[0][0]] = current_group
                current_group = []
            current_group.append((angle, distance))
        # last group
        if current_group:
            if abs(((sorted_objects[0][0] - sorted_objects[-1][0] + math.pi) % (
                    2 * math.pi) - math.pi)) < angle_threshold and grouped_objects:
                first_group = grouped_objects.get(sorted_objects[0][0])
                grouped_objects[sorted_objects[0][0]] = first_group + current_group
            else:
                grouped_objects[current_group[0][0]] = current_group

        return grouped_objects

    # calculate median angle and average distance of grouped scans, so there is only a single point of an object left
    # median of angle will fail, if two angles exactly between -pi and +pi
    # technically the median is not the average, but works better with positive and negative numbers in the same list
    # even though the works almost identically for the angle list
    def average_out_object(self, grouped_objects: dict) -> list:
        average_objects = []
        for values in grouped_objects.values():
            angles, distances = zip(*values)
            average_objects.append((median(angles), mean(distances)))
        return average_objects

    # calculate the length of a group (which essentially is one object)
    # self.calculate_object_group_length(next(iter(grouped_objects.values())))
    def calculate_object_group_length(self, group: list) -> float:
        if not group:
            return 0.0
        angle1, distance1 = group[0]
        angle2, distance2 = group[-1]
        # calculate length of an object using trigonometry
        length = math.sqrt(
            (distance2 * math.cos(angle2) - distance1 * math.cos(angle1)) ** 2 +
            (distance2 * math.sin(angle2) - distance1 * math.sin(angle1)) ** 2)
        return length

    # calculate the yaw angle of the midpoint of the group (which essentially is one object)
    # list(group.values())[0] = group
    def calculate_object_group_yaw(self, group: list) -> float:
        if not group:
            return 0.0
        angle1, distance1 = group[0]
        angle2, distance2 = group[-1]
        x1 = distance1 * math.cos(angle1)
        y1 = distance1 * math.sin(angle1)
        x2 = distance2 * math.cos(angle2)
        y2 = distance2 * math.sin(angle2)
        middle_x = (x1 + x2) / 2
        middle_y = (y1 + y2) / 2
        object_yaw = math.atan2(y2 - middle_y, x2 - middle_x)
        return object_yaw


def main():
    rclpy.init()
    node = RoboControl()
    try:
        rclpy.spin(node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("RoboControl").info("RoboControl node quit!")
    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
