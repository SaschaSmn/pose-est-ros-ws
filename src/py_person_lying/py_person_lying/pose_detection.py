#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
from std_msgs.msg import String
from geometry_msgs.msg import Twist
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

import cv2
from ultralytics import YOLO

import time
from enum import Enum

from robo_control import RoboControl
from src.py_person_lying.pose_est.body import Body


class ImageProcess(Node):
    MODEL_PATH = "./models/yolov8n-pose.pt"
    iy, ix, iz = 640, 640, 3
    FRAMES_UNTIL_MODE_CHANGE = 15
    FRAMES_UNTIL_MOVE = 5
    TEXT_COLOR = (255, 0, 255)
    TEXT_FONT = cv2.FONT_HERSHEY_SIMPLEX

    class State(Enum):
        FALL_DETECT = 0
        LYING_DETECTED = 1
        ROBO_CONTROL = 2
        STANDING = 3
        SPEC_MOVE = 4
        EXIT = 5

    def __init__(self):
        super().__init__("image_process_node")
        time.sleep(1)
        rclpy.logging.get_logger("ImageProcess").info("ImageProcess node started!")

        self.bridge = CvBridge()  # conversion bridge for ros to cv2 images
        self.model = YOLO(self.MODEL_PATH)
        self.bodyclass = Body([], [], True)
        self.mode = ImageProcess.State.FALL_DETECT
        self.arm_pose = Body.ArmPose.NONE
        self.same_arm_pose_frames = 0

        self.fps_smoothing = 0.8  # 0.0 to 1.0
        self.last_fps = 0

        self.init_communications()

    # init ros communication
    def init_communications(self):
        # only keep last. so no messages jam jup
        best_effort_qos_policy = QoSProfile(
            history=HistoryPolicy.KEEP_LAST,
            depth=1,
            reliability=ReliabilityPolicy.BEST_EFFORT
        )
        reliable_qos_policy = QoSProfile(
            history=HistoryPolicy.KEEP_LAST,
            depth=6,
            reliability=ReliabilityPolicy.RELIABLE
        )

        self.robo_mode_publisher = self.create_publisher(
            String,
            "/mode",
            reliable_qos_policy
        )
        self.robo_box_align_publisher = self.create_publisher(
            String,
            "/box_align",
            QoSProfile(depth=10, reliability=ReliabilityPolicy.BEST_EFFORT)
        )
        self.robo_arm_pose_publisher = self.create_publisher(
            String,
            "/arm_pose",
            reliable_qos_policy
        )
        # not needed anymore, but if further additions are planned, leave it here
        self.twist_robo_control_controller_publisher = self.create_publisher(
            Twist,
            "/specified_movement",
            reliable_qos_policy
        )

        self.image_subscriber = self.create_subscription(
            Image,
            "/oakd/rgb/preview/image_raw",
            self.image_callback,
            best_effort_qos_policy
        )

    # switch modes internally and publish to robo node
    def switch_modes(self, mode: State):
        msg = String()
        self.mode = mode
        if mode == ImageProcess.State.FALL_DETECT:
            msg.data = RoboControl.State.EXPLORE.name
        elif mode == ImageProcess.State.ROBO_CONTROL:
            msg.data = RoboControl.State.POSE_CONTROL.name
        elif mode == ImageProcess.State.LYING_DETECTED:
            msg.data = RoboControl.State.DRIVE_TO_BOX.name
        elif mode == ImageProcess.State.STANDING:
            msg.data = RoboControl.State.STAND.name
        elif mode == ImageProcess.State.SPEC_MOVE:
            msg.data = RoboControl.State.SPEC_MOVE.name
        elif mode == ImageProcess.State.EXIT:
            msg.data = RoboControl.State.EXIT.name
            self.robo_mode_publisher.publish(msg)
            raise SystemExit
        rclpy.logging.get_logger("ImageProcess").info(f"Switched mode to: {mode.name}")
        self.robo_mode_publisher.publish(msg)

    # callback for image subscriber and logic for mode / steering the process
    def image_callback(self, msg: Image):
        start_time = time.time()
        if msg is None:
            rclpy.logging.get_logger("ImageProcess").info("failed to fetch image")
            return
        # Convert ros image to cv image
        cv_image = self.bridge.imgmsg_to_cv2(msg, desired_encoding="bgr8")
        # Scale image to 640 * 640
        frame = cv2.resize(cv_image, (self.ix, self.iy))
        # Let the YOLO model predict. Track mode with persisting is used. Also, 1 detection max -> makes most sense
        # TODO test frame for track / predict mode
        results = self.model.track(source=frame, max_det=1, persist=True, conf=0.5, verbose=False)
        # Display mode in frame
        cv2.putText(frame, self.mode.name, (0, self.iy - 5), self.TEXT_FONT, 1, self.TEXT_COLOR, 2, cv2.LINE_AA)

        # TODO further improvement:
        #  what if feet first detected -> need to switch to other drive mode?
        #  if using depth cam this wont be a problem.
        #  else robo will just drive on and eventually find right angle
        #  could drive to person whenever detected, but i dont know if really needed

        boxes = results[0].boxes
        keypoints = results[0].keypoints

        # If model detected points and box => means person detected
        if boxes and keypoints:

            # Feed box and points into body class; get body pose
            self.bodyclass.set_points_box(keypoints[0].xy[0], boxes[0].xyxy[0])
            x1, y1, x2, y2 = self.bodyclass.box
            body_pose = self.bodyclass.get_body_pose_enum()
            text = body_pose.name

            # Display special bbox of the person -> this is a box that removes the arms in a width perspective
            values = self.bodyclass.get_box_box_without_arms()
            coords = ((values[0], values[1]), (values[2], values[3]))
            cv2.rectangle(frame, coords[0], coords[1], self.TEXT_COLOR)
            text_size = cv2.getTextSize(text, self.TEXT_FONT, 1, 2)[0]
            text_x = int((x2 - x1) / 2 + x1 - (text_size[0] / 2))
            # Display pose
            cv2.putText(frame, text, (text_x, y1), self.TEXT_FONT, 1, self.TEXT_COLOR, 2, cv2.LINE_AA)

            """
            if detected person is lying always switch to fall_detect and EXPLORE mode, no matter what happened prior
            if a lying person was detected, drive up to it and set marker to position of person
             and switch back to fall_detect and EXPLORE mode (whereby imageprocess class only publishes box data)
            if detected person is standing and the mode is not robo_control get pose 
            -> if pose is p1 (90|90) (-|-) make robo standing
            if person does not continue to hold p1 than switch modes to fall_detect again and move on.
            else if person holds p1 for long enough than switch to robo_control.
            if in robo_control detect poses -> if poses are hold for long enough than do "things" 
            (like follow of do s a spin)
            p7 for follow (UP UP)
            if in robo_control p1 is hold for long enough switch back to fall detect mode
            """
            if self.mode == ImageProcess.State.LYING_DETECTED:
                self.lying_detected_flow()

            elif self.mode == ImageProcess.State.ROBO_CONTROL:
                self.robo_control_flow(frame)

            elif body_pose == Body.BodyPose.LYING:
                self.lying_flow()

            elif body_pose == Body.BodyPose.STANDING:
                self.standing_flow()

        # TODO further improvement:
        #  this will result in an endless loop if no person is detected and robo in pose control mode
        #  so will have to end mode via arm pose
        #  if robo in front of wall with cam -> that is the only problem
        elif self.mode == ImageProcess.State.ROBO_CONTROL:
            pass

        # if no person detected move on with fall_detect and explore mode
        else:
            if self.mode != ImageProcess.State.FALL_DETECT:
                self.switch_modes(ImageProcess.State.FALL_DETECT)

        end_time = time.time()
        self.calculate_and_display_fps(frame, start_time, end_time)
        self.show_frame(frame, 1.2)

    # process if in lying detected mode -> trying to set marker on rviz map
    def lying_detected_flow(self):
        x1, y1, x2, y2 = self.bodyclass.box
        msg = String()
        msg.data = ",".join(map(str, [x1, y1, x2, y2]))
        self.robo_box_align_publisher.publish(msg)
        # technically should switch mode when robo turns, because no box
        # switching mode here could result in mode switch in robo control before marker was set

    # process if lying person was detected -> switch mode for constant mode
    def lying_flow(self):
        # TODO further improvement:
        #  add other stuff like driving around person to see from side not front or behind angle
        self.switch_modes(ImageProcess.State.LYING_DETECTED)

    # process if in robo control mode -> recognize arm poses
    def robo_control_flow(self, frame):
        x1, y1, x2, y2 = self.bodyclass.get_box_box_without_arms()
        new_arm_pose = self.bodyclass.get_arm_pose_enum()
        new_arm_pose_text = new_arm_pose.name
        cv2.putText(frame, new_arm_pose_text, (x2, y1 + 30), self.TEXT_FONT, 1, self.TEXT_COLOR, 2,
                    cv2.LINE_AA)
        if new_arm_pose == Body.ArmPose.NONE:
            return
        if new_arm_pose == self.arm_pose:
            self.same_arm_pose_frames += 1
            if self.same_arm_pose_frames >= self.FRAMES_UNTIL_MOVE:
                if new_arm_pose == Body.ArmPose.SIDE_SIDE:
                    self.switch_modes(ImageProcess.State.FALL_DETECT)
                elif new_arm_pose == Body.ArmPose.DOWN_DOWN:
                    pass  # could pub - no problem, but will fill pose list
                else:
                    msg = String()
                    msg.data = new_arm_pose_text
                    self.robo_arm_pose_publisher.publish(msg)
                    print("pub pose " + new_arm_pose_text)
                self.same_arm_pose_frames = 0
        else:
            self.same_arm_pose_frames = 0
        self.arm_pose = new_arm_pose

    # process if standing was detected -> look if certain pose => if so switch mode
    def standing_flow(self):
        new_arm_pose = self.bodyclass.get_arm_pose_enum()
        # only stop if arm pose is SIDE_SIDE
        if new_arm_pose == Body.ArmPose.SIDE_SIDE:
            if self.mode != ImageProcess.State.STANDING:
                self.switch_modes(ImageProcess.State.STANDING)
            self.same_arm_pose_frames += 1
            if self.same_arm_pose_frames >= self.FRAMES_UNTIL_MODE_CHANGE:
                self.switch_modes(ImageProcess.State.ROBO_CONTROL)
                self.same_arm_pose_frames = 0
        # drive on in fall_detect mode if you did not hold arm pose for long enough
        elif self.mode == ImageProcess.State.STANDING:
            self.same_arm_pose_frames = 0
            self.switch_modes(ImageProcess.State.FALL_DETECT)

    def calculate_and_display_fps(self, frame, start_time, end_time):
        current_fps = 1 / (end_time - start_time)
        fps = (current_fps * self.fps_smoothing) + (self.last_fps * (1 - self.fps_smoothing))
        self.last_fps = current_fps
        cv2.putText(frame, str(int(fps)), (0, 25), self.TEXT_FONT, 1, self.TEXT_COLOR, 1, cv2.LINE_AA)

    # size: 1.0 = normal, 1.1 bigger, ..
    def show_frame(self, frame, size: float):
        # resize frame
        frame = cv2.resize(frame, (int(self.ix * size), int(self.iy * size)))
        # show window and quit with "q"
        cv2.imshow("cam", frame)
        if cv2.waitKey(1) & 0xFF == ord("q"):
            cv2.destroyAllWindows()
            self.switch_modes(ImageProcess.State.EXIT)


def main():
    rclpy.init()
    node = ImageProcess()
    try:
        rclpy.spin(node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("ImageProcess").info("ImageProcess node quit!")
    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
