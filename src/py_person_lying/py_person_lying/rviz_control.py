#!/usr/bin/env python3

import rclpy
from rclpy.node import Node
from rclpy.duration import Duration
from rclpy.qos import QoSProfile, ReliabilityPolicy, HistoryPolicy
from std_msgs.msg import String, Float32
from geometry_msgs.msg import Twist, Pose, Quaternion, Point, PoseWithCovarianceStamped
from visualization_msgs.msg import Marker
from tf_transformations import euler_from_quaternion, quaternion_from_euler

import time
import math


class RvizController(Node):
    MARKER_DISTANCE = 1.05
    MARKER_LENGTH = 1.75
    MARKER_SIMILAR_DISTANCE = 0.7

    def __init__(self):
        super().__init__("rviz_control_node")
        time.sleep(1)
        rclpy.logging.get_logger("RvizController").info("RvizController marker node started!")
        self.pose_data = Pose()
        self.markers = []

        self.init_communications()

    def init_communications(self):
        pose_qos_policy = QoSProfile(
            reliability=ReliabilityPolicy.BEST_EFFORT,
            history=HistoryPolicy.KEEP_LAST,
            depth=2
        )
        reliable_qos_policy = QoSProfile(
            history=HistoryPolicy.KEEP_LAST,
            depth=6,
            reliability=ReliabilityPolicy.RELIABLE
        )

        self.marker_rviz_publisher = self.create_publisher(
            Marker,
            "/visualization_marker",
            reliable_qos_policy
        )

        self.pose_subscriber = self.create_subscription(
            PoseWithCovarianceStamped,
            "/amcl_pose",
            self.pose_sub_callback,
            pose_qos_policy
        )
        self.image_process_marker_subscriber = self.create_subscription(
            String,
            "/rviz_marker",
            self.publish_marker,
            reliable_qos_policy
        )

    # pose data callback from amcl_pose (adaptive monte carlo localization)
    def pose_sub_callback(self, pose_msg: PoseWithCovarianceStamped):
        self.pose_data = pose_msg.pose.pose

    # get basic marker and publish to a topic, thus publishing to the rviz map, if there is not another marker near
    def publish_marker(self, marker_data: String):
        # no pose data
        if self.pose_data == Pose():
            rclpy.logging.get_logger("RvizController").info(
                "Trying to set marker without pose data being set. Marker won't be placed")
            return
        marker = self.__get_basic_marker(marker_data)
        rclpy.logging.get_logger("RvizController").info("Trying to set marker")
        if not self.is_similar_marker_existing(marker):
            # check the number of markers and set id accordingly, so that no marker is overwritten
            marker.id = len(self.markers)
            self.marker_rviz_publisher.publish(marker)
            rclpy.logging.get_logger("RvizController").info("Marker placement success")

            quaternion = [self.pose_data.orientation.x, self.pose_data.orientation.y, self.pose_data.orientation.z,
                          self.pose_data.orientation.w]
            (x_roll, y_pitch, z_yaw) = euler_from_quaternion(quaternion)
            print(f"Robo at:")
            print(
                f"position: x: {self.pose_data.position.x}; y: {self.pose_data.position.y}; z: {self.pose_data.position.z}")
            print(f"orientation: yaw: {z_yaw}\n")

            quaternion = [marker.pose.orientation.x, marker.pose.orientation.y, marker.pose.orientation.z,
                          marker.pose.orientation.w]
            (x_roll, y_pitch, z_yaw) = euler_from_quaternion(quaternion)
            print(f"Marker {marker.id} placed at:")
            print(f"position: x: {marker.pose.position.x}; y: {marker.pose.position.y}; z: {marker.pose.position.z}")
            print(f"orientation: yaw: {z_yaw}\n\n")
            self.markers.append(marker)
        else:
            rclpy.logging.get_logger("RvizController").info("Marker placement failure")

    # check if distance from new to other markers is bigger than a certain threshold
    # this is done by calculating the absolute distance between positions
    def is_similar_marker_existing(self, marker: Marker, distance_threshold: float = MARKER_SIMILAR_DISTANCE) -> bool:
        similar_existing = False
        if self.markers:
            mapped_marker_diff = map(
                lambda m: (abs(m.pose.position.x - marker.pose.position.x) +
                           abs(m.pose.position.y - marker.pose.position.y)) < distance_threshold,
                self.markers)
            similar_existing = any(list(mapped_marker_diff))
        print(f"Checked for similar marker.\n Is existing?: {similar_existing}\n")
        return similar_existing

    # returns a marker with position data of person
    def __get_basic_marker(self, marker_data: String) -> Marker:
        marker_data_list = marker_data.data.split(",")
        marker_distance, marker_yaw, marker_length = list(map(float, marker_data_list))
        # print(f"dist {marker_distance}; yaw {marker_yaw}; length {marker_length}\n")

        marker = Marker()
        robo_position = self.pose_data.position
        robo_orientation = self.pose_data.orientation
        marker.header.frame_id = "map"
        marker.header.stamp = self.get_clock().now().to_msg()

        # set shape, Arrow: 0; Cube: 1; Sphere: 2; Cylinder: 3 ...
        marker.type = 1
        # 0 = add / modify; 2 = delete; 3 = deleteall
        marker.action = 0
        # 0 seconds is forever
        marker.lifetime = Duration(seconds=0).to_msg()

        # set the scale of the marker
        marker.scale.x = 0.2
        marker.scale.y = marker_length if (
                marker_length is not None and marker_length != 0.0 and marker_length <= 2.0) else self.MARKER_LENGTH
        marker.scale.z = 0.2

        # set the color
        marker.color.r = 0.0
        marker.color.g = 1.0
        marker.color.b = 0.0
        marker.color.a = 1.0

        """
        unstable and very fragile. best do not use

        # Set the orientation of the marker
        if yaw is not None and yaw != 0.0:
            quaternion = quaternion_from_euler(0.0, 0.0, yaw)
            print(f"quaternion: {quaternion}\n")
            marker_orientation = Quaternion()
            marker_orientation.x = quaternion[0]
            marker_orientation.y = quaternion[1]
            marker_orientation.z = quaternion[2]
            marker_orientation.w = quaternion[3]
        marker.pose.orientation = marker_orientation
        """
        marker.pose.orientation = robo_orientation

        marker_distance = marker_distance if (
                marker_distance is not None and marker_distance != 0.0 and marker_distance < self.MARKER_DISTANCE) \
            else self.MARKER_DISTANCE

        quaternion = [robo_orientation.x, robo_orientation.y, robo_orientation.z, robo_orientation.w]
        # convert quaternion to euler
        (x_roll, y_pitch, z_yaw) = euler_from_quaternion(quaternion)
        print(f"\ndist {marker_distance}; yaw {robo_orientation}; length {marker.scale.y}")

        # yaw is used for orientation
        marker_distance_x = marker_distance * math.cos(z_yaw)
        marker_distance_y = marker_distance * math.sin(z_yaw)
        print("cos dist " + str(marker_distance_x))
        print("sin dist " + str(marker_distance_y))
        marker.pose.position.x = robo_position.x + marker_distance_x
        marker.pose.position.y = robo_position.y + marker_distance_y
        marker.pose.position.z = 0.0
        return marker


def main():
    rclpy.init()
    node = RvizController()
    try:
        rclpy.spin(node)
    except (SystemExit, KeyboardInterrupt):
        rclpy.logging.get_logger("RvizController").info("RvizController marker node quit!")
    finally:
        node.destroy_node()
        rclpy.shutdown()


if __name__ == "__main__":
    main()
