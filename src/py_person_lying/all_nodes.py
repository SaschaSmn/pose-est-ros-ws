#!/usr/bin/env python3

import rclpy

from pose_detection import ImageProcess
from robo_control import RoboControl
from rviz_control import RvizController

from rclpy.executors import MultiThreadedExecutor
import threading


def main(args=None):
    rclpy.init(args=args)
    try:
        image_process = ImageProcess()
        robo_control = RoboControl()
        rviz_control = RvizController()

        executor = MultiThreadedExecutor()
        executor.add_node(image_process)
        executor.add_node(robo_control)
        executor.add_node(rviz_control)

        try:
            executor_thread = threading.Thread(target=executor.spin, daemon=True)
            executor_thread.start()
            # executor.spin()
        except (SystemExit, KeyboardInterrupt):
            rclpy.logging.get_logger("Quitting").info('Nodes quit!')
        finally:
            executor.shutdown()
            image_process.destroy_node()
            robo_control.destroy_node()
            rviz_control.destroy_node()

    finally:
        rclpy.shutdown()


if __name__ == '__main__':
    main()
