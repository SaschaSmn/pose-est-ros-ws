# Detecting lying persons from keypoints provided by the YOLOv8 pose model and marking them on a map, done by a Turtlebot 4

Alternate titels in german:  
- Erkennung und Verortung von liegenden Personen mit einem Turtlebot 4 und einem Pose Neuronalen Netz  
- Algorithmische Erkennung von liegenden Personen auf Basis von Punkten aus einem Pose Neuronalen Netz (YOLOv8), und Verortung dieser mit einem Turtlebot 4.

## Important Notice
**FOR FURTHER REFERENCE, PLEASE READ THE FULL DOCUMENTATION, AS THIS README IS JUST TO PROVIDE AN OVERVIEW
AND MIGHT NOT BE UP TO DATE**  
*If you are interested in the project, please contact me for further information*  

The branch that should be used is the **ros_run** branch, as testing was done with code from this branch.  
The code from main branch should work fine, but not with '*ros2 run py_person_lying ...*'.  
But there is also no guarantee, that it is 100% bug-free, as it is untested.

## What does this project do and what does it contain
This project is the main workspace for everything ros2 related for the project https://gitlab.com/SaschaSmn/pose-est  
This workspace allows running ros2 nodes via *ros2 run ..*  
Though it is to be said that this has become the main project.  
The 'pose-est' repository stores all the finished code

This project was built by Sascha Sauermann,
as his Praxisprojekt as a part of his Bachelors degree in Allgemeine Informatik,
a bachelor programme provided by the TH Köln
(Campus Gummersbach).  
**Do not copy code or use this code for monetary gain, unless you are allowed by the owner**

The main intent of the project is a concept of a 'detect and mark' robot.  
The named robot is a *Turlebot4* from *Clearpath Robotics, Inc.*;  
This is a robot that is set together from an iRobot® Create3 mobile base,
a powerful Raspberry Pi 4 running ROS 2, OAK-D spatial AI stereo camera, 2D LiDAR.

The robot roams a certain room, that is mapped beforehand.
When roaming around it searches for persons lying down.
If one person is detected, it will be marked on a map.
The said map is available via rviz.



## How does the communication between computer and robot work
All ros nodes run on the computer. 
The robot and computer must be in the same network.  
Now the robot and computer both subscribe and publish to certain message types.  
These message types are subscribed / published on a certain topic.  
An example would be a message named 'Twist' that contains velocity data. 
This data can be send out to the robot from the computer, under the topic '/cmd_vel'.
This will tell the robot to do a certain movement.  
Another example would be the image data from the OAKD camera. These images will be published under a topic and the ImageProcess node is subscribed to it.  
![overview](./content/doc/overview_parts_complete.jpg)



## How does the detection of lying persons work
First a class was built. The 'Body' class.  
This class takes the detections of the yolov8 pose model, namely the nano model.  
These detections are the box and the detected keypoints as x and y coordinates as tuples.  
These keypoints accord to the 17 COCO keypoints for humans. 
Note, that the class only takes the detetctions of a single prediction.  
Though it is possible to enable persisting, which will save the n last results of predictions.  
If lets say 4/6 positions were classified as lying, 
matching a certain threshold (e.g. over 50%), the result will be lying.  
The track mode of the yolo model is used, so that moving objects will have some sort of consistency and not every prediction is made without an existing bias.
This should help to keep persons detected in every prediction frame.

The OAKD camera provides the ImageProcess Node with a stream of images. 
These images are then analysed by the yolo model.
If now a person was detected, then the box and keypoints are set in the bodyclass.  
otherwise the robot will drive on and roam the room, searching for humans.  
Now a query is made to the bodyclass, asking if the person is lying.
If so the robot will drive up to the person.  
If the bounding box of the person covers a certain percentage of the image, 
the person will be recognized as'full in frame'. 
Now the RoboControl node will tell the RvizController node to set a marker on the rviz map on these certain coordinates.

If the robot detects a standing person doing a special pose, it will stop and look at the person.
If the person holds this pose for long enough, the robot will switch to 'robo control mode'.


## How are poses of persons classified
After trying different methods, the method I settled on works fairly well.
As mentioned box and keypoint data is given to the Body class.  
First the keypoints are taken and an angle between shoulder and hip is calculated.  
If the angle between those two points is in a certain range,
a person will either be considered **(if enabled) 'sitting', 'kneeling', 'planking'
'standing', 'lying' or 'upside down'**.  
The positions that can be switched on/off are: **'sitting', 'kneeling' and 'planking'**.  
If none of said points is detected, first upper body points, then lower body points will be tested for angles.
From these angles, the classification is also possible.  
If none of the essential points is available, it will return **'no essential points'**.  
At this moment a classification is still possible, 
but would go against my will, of the algorithm working in a respectable manner.  
In some complicated cases, a fail-safe method will be called. 
This method mainly works with the bounding box and its length and width.
It will probably result in the classification **'no full body shown'**.   
If not, and safety measures are ignored (e.g. no essential points available), it will probably result in a simple classification like **'lying', 'standing' or 'upside down'**.


## Robo control mode
In robo control mode, you will be able to do poses. 
This poses are done with both arms.  
Arm angles in ratio to the body (to be precise an angle ratio between elbow and shoulder) will be calculated.  
Available poses are already set:  

* **l|r :** 
* pose1 : -|- 0|0; 
* pose2: /|\ -45|-45; 
* pose3: \|/ 45|45; 
* pose4: \|\ 45|-45; 
* pose5: /|/ -45|45; 
* pose6: |°| -90|-90;
* pose7: |.| 90|90

Hold the pose for a certain time. p1 will enable and disable robo control mode.  
p2 will let the robot do a full 360 turn in right rotation.  
p7 will enable follower mode. 
To disable reach near the lidar sensor


## Follower mode
Currently, the ImageProcess node will stay in ROBO_CONTROL mode and the RoboControl node will enter FOLLOWER mode.  
The FOLLOWER mode is quit, by a detected laserscan with a range below a threshold e.g. 0.1.  
The follower movement is calculated in a similar way to the explore movement.  
The difference is that we want to follow an object and in explore mode, we want to avoid objects.  
First, we go ahead and take the laserscan information. 
This is a Lidar sensor that provides 360° scans; each measurement results in a scanned distance.  
Now we take these measurements, and for every scan, it is checked if distance is within the 'avoidance distance'.  
So distance values that represent an infinite distance or incorrect measurements are ignored.  
If the distance measurement at this said angle is within the 'minimal follower distance'
and the 'avoidance distance', it will be saved.  
This will mean, only scans with a distance between e.g. 0.2 and 0.3 meters are saved for the follower movement calculations.
The scans will be filtered, so that only scans, that happened in the front 180° of the robot will be saved.  
The scans will be grouped into estimated objects. The midpoint of these objects will be calculated and taken into consideration for the calculation of the driving angle.  
Now the angular velocity of the robot will be set to the angle of the first detected 'object' (the person);   
With this being the first lidar scan, which distance was in the detection threshold.  
So we just drive in the direction of the object.
The robot will stop if the object/person is too close to the robot.  
This won't result in a 'quit' of follower mode, as the quitting distance is lower, than the stopping distance.


## How does the movement system function
As already mentioned in the explanation of the follower movement,
the calculation for the movements of the explore movement is calculated in a similar manner.  
First every scan from the lidar sensor with its distance and angle are taken through a filter.
Distance values that are unrealistic are ignored and only those that fall within a detection distance,
are saved as a pair with the according angle of the scan.  
Now this pair of angle and distance are filtered again,
so that only the scans remain, which were taken from the 180° front of the robot.
Scans behind the robot normally can be ignored, at least from a logical standpoint; concerning a robot,
that only drives forward.  
These scans will now be grouped into 'object groups'. If at least two scans are within a certain angle next to each other and both were not filtered out,  
they will build a group. A group can contain one or more scan.
If a distance was scanned,
that is below a minimal distance, the robot will stop its linear movement and will only do angular movement on the spot,
until the front is clear from objects too close.
This minimal distance check happens with the lidar sensor and an infrared sensor, that is in the front bumper of the robot.  
If no distance was scanned below the detection distance, the robot will just drive in a straight line;
Straight being straight in the direction the robot was already orientated.  
The scans are split in two lists.
One list containing the scans of the front left side, the other containing the front right scans.
These right side list is sorted biggest to smallest (-1.6,-2,-2.2), the left side list from smallest to biggest(-1.3,-1.1,-0.9).  
Let's say there are only scans in the left list.
Which would be from 0 to -pi/2 in Radians.  
The first element could be (-1.0, 0.25).
The angular movement will be exactly 90° degrees in the other direction, which would be ~0.57 in this case.
So 90° are added for the left side elements and 90° subtracted for right-hand elements.  
If there are elements in the left side list and the right side list, 
then from both lists the first element will be taken and the average of these two values will be calculated. 
This should result in the robot driving through a line in between the two objects/detections.  
There should be no problem in this, because the robot will stop and turn if it is too close to an object.  
<p float="left">
	<img src="./content/doc/explore_movement_right.png" alt="drawing" width="520"/>
	<img src="./content/doc/explore_movement_left_right.png" alt="drawing" width="520"/>
</p>  
<p float="left">
	<img src="./content/doc/explore_movement_min.png" alt="drawing" width="520"/>
	<img src="./content/doc/explore_movement_min_twist.png" alt="drawing" width="520"/>
</p>

## How are markers placed on the map
The robot detects a lying person and will drive up to a certain point to the person.
The robot will follow the direction of the middle of the bounding box, 
as the bounding box coordinates are constantly published by the image process node to the robo control node.  
If the bounding box person fills a percentage of the screen/image (e.g. 5/6 (~83%) of the width and 1.75/2.75 (~63%) of the height (53% of the screen)),
the robo control node will command the rviz controller to place a marker.
First, a basic marker is built, based on the position and orientation of the robot.
This basic marker is further specified by data, like the exact distance of the person to the robot (from lidar) and the length of the person.  
After that, a check happens if there already is a marker nearby, to avoid double placements.
If none is detected, the marker will be placed.
Now the robot will switch to explore mode and try to avoid objects (in this case the person).
It should turn around and drive on, if the person is finally out of the camera frame, the image process node will also switch back to explore mode, 
**not any earlier** as this would result in an endless loop.  


** This does not work at the moment**
*As depth images are not published by the robot in the current state,  
if you are interested in developing this further yourself, I advice you to look at the different branches*
The **alternate version** works the following way:  
The robot detects a lying person. 
(x,y) points are taken from the keypoints.
From a depth image, the distance will be taken.
Now the average distance will be calculated. 
The average distance will be transmitted to the rviz controller node.
Now based on the distance and the orientation, the marker position will be calculated.
If a similar marker is existing, it won't be placed.
In the other case, the marker will be placed and the robot drives on.



## Requirements
Please follow the instructions from the official turtlebot4 documentation.
https://turtlebot.github.io/turtlebot4-user-manual/setup/basic.html  
This will ensure you installed everything considering ros2 and humble, so that you are able to run rclpy code.  
You probably will have to install tf transformations for ros2 via:  
*sudo apt install ros-humble-tf-transformations*  
Trying to run a ros2 environment in conda will probably fail.  
If conda is enabled, please deactivate it, via: *conda deactivate*  
Other requirements are OpenCV and ultralytics as noted in requirements.txt



## What to do to run (specific to my setup)
- Open 5 to 6 Terminals.  
You can do this with "*terminator*"
- Go to ~Dokumente/sascha_robo/pose-est-ros-ws-main (or wherever your repo is)
- Be sure *colcon build* was done. or *colcon build --symlink-install*
- If you did colcon build, be sure to source install again:  
   - *source /opt/ros/humble/setup.bash && source /usr/share/colcon_argcomplete/hook/colcon-argcomplete.bash && source ~/Dokumente/sascha_robo/pose-est-ros-ws-main/install/setup.bash && source ~/Dokumente/sascha_robo/pose-est-ros-ws-main/install/local_setup.bash*  
  - you need to do this in every terminal you opened after colcon build. unless you opened new terminals after colcon build and your bashrc is set up accordingly
- Set up rviz components, run this in sascha_robo/pose-est-ros-ws-main/content (or wherever your map is located):
  - *ros2 launch turtlebot4_navigation localization.launch.py map:=labor1242.yaml* (or 2labor1242.yaml for better map)
  - - optional *ros2 launch turtlebot4_navigation nav2.launch.py*
  - *ros2 launch turtlebot4_viz view_robot.launch.py*
  - you can open the config.rviz in /Dokumente/sascha_robo/pose-est-ros-ws-main/content, this is because Marker Topic subscription is needed
  - and be sure to set the initial position of the robot in rviz (as accurate as possible)
- Start the files. Either python3 'filename.py' or:
  - *ros2 run py_person_lying image_node*
  - *ros2 run py_person_lying robo_node*
  - *ros2 run py_person_lying marker_node*
- Currently not working:  
  - alternatively, you can do *ros2 launch py_person_lying ppl_launch.launch.py* for the 3 nodes
  - or (maybe) *ros2 run py_person_lying all_nodes*