from launch import LaunchDescription
from launch_ros.actions import Node
 
def generate_launch_description():
    ld = LaunchDescription()
    image_node = Node(
        package="py_person_lying",
        executable="image_node",
        name='img'
    )
    robo_node = Node(
        package="py_person_lying",
        executable="robo_node",
        name='robo'
    )
    marker_node = Node(
        package="py_person_lying",
        executable="marker_node",
        name='marker'
    )
 
    ld.add_action(image_node)
    ld.add_action(robo_node)
    ld.add_action(marker_node)
    return ld
